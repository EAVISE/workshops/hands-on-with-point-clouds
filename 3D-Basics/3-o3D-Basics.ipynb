{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Open3D Basics\n",
    "In this first section, the necessary groundwork is laid for the sample application of this workshop.\n",
    "Mainly, library functions will be explained to perform the following actions:\n",
    "1. File Import/Export (File I/O)\n",
    "2. Visualisation\n",
    "3. KDTree / Octree\n",
    "4. Voxel Grids / Voxelisation\n",
    "5. Outlier/Noise Removal\n",
    "\n",
    "After going though all these step by step, the result will be an interactive Python script to import point clouds segments for the workshop example, and prepare them for the alignment process by removing noise artifacts from the point-cloud."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###### Import Required Libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import open3d as o3d\n",
    "import numpy as np\n",
    "import os\n",
    "import sys"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.1 File IO\n",
    "Open3D supports importing and exporting three main filetypes:\n",
    "- Point-clouds\n",
    "- Triangle meshes\n",
    "- Images"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### File Import\n",
    "#### Point cloud\n",
    "Reading a point-cloud from a file can be done with the line of code below. The usage of this function is quite simple:\n",
    "\n",
    "It requires a string argument specifying the file (and path). This can be either an absolute path, or a path relative to the current directory. The function returns a handle to the data stored in memory, in the case below, the point cloud loaded from the file \"CloudSample.pcd\" is used throughout the program with the handle *pcd*. \n",
    "\n",
    "**Note:** The used function `read_point_cloud()` belongs to the `io` module of the Open3D library imported as `o3d`, requiring the syntax below to call."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pcd = o3d.io.read_point_cloud(\"../data/open3D-Data/CloudSample.pcd\") # "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Open3D's import functionality is user-friendly in that it recognises a variety of file formats, and interprets the contents accordingly. By default, the file extensions are used for this purpose. The table below shows the compatible formats (and the associated file extensions).\n",
    "\n",
    "Format   | Description\n",
    "---------|---------------\n",
    "`xyz`    | Each line contains `[x, y, z]`, where `x`, `y`, `z` are the 3D coordinates\n",
    "`xyzn`   | Each line contains `[x, y, z, nx, ny, nz]`, where `nx`, `ny`, `nz` are the normals\n",
    "`xyzrgb` | Each line contains `[x, y, z, r, g, b]`, where `r`, `g`, `b` are in floats of range `[0, 1]`\n",
    "`pts`    | The first line is an integer representing the number of points. The subsequent lines follow one of these formats: `[x, y, z, i, r, g, b]`, `[x, y, z, r, g, b]`, `[x, y, z, i]` or `[x, y, z]`, where `x`, `y`, `z`, `i` are of type `double` and `r`, `g`, `b` are of type `uint8`\n",
    "`ply`    | See [Polygon File Format](http://paulbourke.net/dataformats/ply), the ply file can contain both point cloud and mesh data\n",
    "`pcd`    | See [Point Cloud Data](http://pointclouds.org/documentation/tutorials/pcd_file_format.html)\n",
    "\n",
    "**Note:** with the exception of *ply* and *pcd* formats, the other filetypes are simple ASCII text files, which may more often than not be stored simply with the *.txt*-extension. For this reason, it is also possible to specify the file type explicitly. In this case, the file extension will be ignored."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pcd2 = o3d.io.read_point_cloud(\"../data/open3D-Data/CloudSample.txt\", format='xyz')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Open3D's internal point-cloud datatype contains not only the raw data imported from a file, but also some computed metadata. As a consequence, the size of a point-cloud can be gathered with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(pcd) # Print the datatype and size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Mesh\n",
    "Triangle meshes differ from point-clouds in that the triangles define 3D surfaces. For point-clouds, the raw data does not contain information which points are connected (i.e., belong to one surface segment).\n",
    "\n",
    "Using mesh filetypes in Open3D is very similar to point-clouds. Several formats are supported, as shown in the table. The type will by default be inferred from the file extension. Analogous to point cloud imports, it can also be specified via the `format` argument, ignoring the file extension.\n",
    "\n",
    "Format          | Description\n",
    "----------------|---------------\n",
    "`ply`           | See [Polygon File Format](http://paulbourke.net/dataformats/ply/), the ply file can contain both point cloud and mesh data\n",
    "`stl`           | See [StereoLithography](http://www.fabbers.com/tech/STL_Format)\n",
    "`obj`           | See [Object Files](http://paulbourke.net/dataformats/obj/)\n",
    "`off`           | See [Object File Format](http://www.geomview.org/docs/html/OFF.html)\n",
    "`gltf`/`glb`    | See [GL Transmission Format](https://github.com/KhronosGroup/glTF/tree/master/specification/2.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mesh = o3d.io.read_triangle_mesh(\"../data/open3D-Data/MeshSample.stl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Furthermore, also the Open3D-internal mesh datatype contains readily accessible metadata of the imported file, accessed through the `print()`-function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(mesh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Image\n",
    "Open3D also allows importing images, but only the *jpg* and *png* formats. Since it does not provide any particular functions for image processing, this is mainly useful for visualising or analysing RGB-D data. Where actual image processing is required, other libraries like OpenCV are recommended instead.\n",
    "\n",
    "The syntax for importing images is similar to that for point-clouds and meshes, the only important difference being the different function names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img = o3d.io.read_image(\"../data/open3D-Data/ImageSample.jpg\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The image size, same as for point-clouds and meshes, can be accessed with the `print()`-function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(img)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### File Export\n",
    "For all three filetypes, Open3D also offers a function to write processing results back into a file for storage beyond program runtime. Syntactially, the important difference to the import-functions is that exporting data to a file requires to arguments: a string argument specifying the complete file path (including name and extension), and the variable holding the data during program runtime. File export is possible in any of the supported file types, which is inferred only from the extension. The `format` specifier used during import is not implemented for file export.\n",
    "\n",
    "As an example, the point-cloud imported earlier can be written back into another file with the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o3d.io.write_point_cloud(\"../data/open3D-Data/CopiedSample.pcd\", pcd) # This file is found in the same directory as the provided samples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The exporting of images and meshes is possible analogously with:\n",
    "`o3d.io.write_triangle_mesh(\"copy_of_knot.ply\", mesh)`\n",
    "`o3d.io.write_image(\"copy_of_lena_color.jpg\", img)`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.2 Visualization\n",
    "In this workshop, only the basics of visualization features contained in Open3D will be introduced. Open3D's `visualization` module offers a variety of functions, from simple display of data to an interactive viewer that allows cropping point-clouds or inspecting individual points.\n",
    "\n",
    "During this workshop, the primary tool will be the `draw_geometries()` function from the `o3d.visualization` module. This function has several arguments, not all of which are required:\n",
    "- geometry_list (List[open3d.geometry.Geometry]) – List of geometries to be visualized.\n",
    "- window_name (str, optional, default='Open3D') – The displayed title of the visualization window.\n",
    "- width (int, optional, default=1920) – The width of the visualization window.\n",
    "- height (int, optional, default=1080) – The height of the visualization window.\n",
    "- left (int, optional, default=50) – The left margin of the visualization window.\n",
    "- top (int, optional, default=50) – The top margin of the visualization window.\n",
    "- point_show_normal (bool, optional, default=False) – Visualize point normals if set to true.\n",
    "- mesh_show_wireframe (bool, optional, default=False) – Visualize mesh wireframe if set to true.\n",
    "- mesh_show_back_face (bool, optional, default=False) – Visualize also the back face of the mesh triangles.\n",
    "- lookat (numpy.ndarray[float64[3, 1]]) – The lookat vector of the camera.\n",
    "- up (numpy.ndarray[float64[3, 1]]) – The up vector of the camera.\n",
    "- front (numpy.ndarray[float64[3, 1]]) – The front vector of the camera.\n",
    "- zoom (float) – The zoom of the camera.\n",
    "\n",
    "The point-cloud file used as an example before looks like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o3d.visualization.draw_geometries([pcd], # more than one point-cloud, mesh and/or image can be shown at the same time\n",
    "                                  zoom=1,\n",
    "                                  front=[0,0,1], # viewing direction of the camera onto the point-cloud\n",
    "                                  lookat=[0, 0, 0], # Cartesian coordinate onto which the view is centered\n",
    "                                  up=[0, 1, 0]) # alignment of image \"up\" direction to point-cloud"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Furthermore, this display window does not have a fixed view. It can be freely rotated, shifted and zoomed, using the following controls:\n",
    "\n",
    "**Mouse view control** \n",
    "  Left button + drag         : Rotate.\n",
    "  Ctrl + left button + drag  : Translate.\n",
    "  Wheel button + drag        : Translate.\n",
    "  Shift + left button + drag : Roll.\n",
    "  Wheel                      : Zoom in/out.\n",
    "\n",
    "**Keyboard view control**\n",
    "  [/]          : Increase/decrease field of view.\n",
    "  R            : Reset view point.\n",
    "  Ctrl/Cmd + C : Copy current view status into the clipboard.\n",
    "  Ctrl/Cmd + V : Paste view status from clipboard.\n",
    "\n",
    "**General control**\n",
    "  Q, Esc       : Exit window.\n",
    "  H            : Print help message.\n",
    "  P, PrtScn    : Take a screen capture.\n",
    "  D            : Take a depth capture.\n",
    "  O            : Take a capture of current rendering settings."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.3 Voxel Grids / Voxelization\n",
    "Point-clouds are an unstrcutured threedimensial format. Points have no defined relationship to neighbours (unlike meshes, which connect points into surfaces), and the distribution of points is not uniform. Voxel grids on the other hand are a uniformly distributed representation of threedimensional data. A voxel grid divides space into (usually evenly-sized) volume units (voxels).  To represent a point-cloud, these voxels can have varying occupancy, depending on the local point densities; they can even by empty.\n",
    "\n",
    "For conversion into a voxel grid, the voxel size is important: Open3D uses cubic voxels, where the `voxel_size` argument of the function is interpreted in the scale of the input point-cloud. Feel free to rerun the code cell below with a different voxel size to visualize the effects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "voxel_grid = o3d.geometry.VoxelGrid.create_from_point_cloud(pcd, voxel_size=1.5) # Larger voxel size to demosntrate efffect\n",
    "o3d.visualization.draw_geometries([voxel_grid], zoom=1, front=[0,0,1], lookat=[0, 0, 0], up=[0, 1, 0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Within the scope of this workshop, voxel grids will be used indirectly, for point-cloud downsampling. Open3D provides two ways of reducing the size of point-clouds:\n",
    "- voxel downsampling\n",
    "- uniform downsampling\n",
    "\n",
    "With uniform downsampling, every n-th point of the cloud is selected and retained for the down-sampled output; all other points are removed. Evidently, with a larger \"n\", fewer points are retained, respectively the point-cloud is reduced in soze more significantly. If desired, this can be experienced by rerunning the code cell below with varying parameter `every_k_points`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uni_down_pcd = pcd.uniform_down_sample(every_k_points=50) # retain one in 50 points\n",
    "o3d.visualization.draw_geometries([uni_down_pcd], zoom=1, front=[0,0,1], lookat=[0, 0, 0], up=[0, 1, 0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For Voxel-based downsampling, the poin-cloud is converted into a voxel grid at the specified voxel size. To obtain the downsampled point-cloud, one point representing each occupied voxel (i.e. voxels containing one or more points) is added to a new point-cloud. Consequently, the larger the used `voxel_size` argument, the greater the reduction. The main difference between this and uniform downsampling is that the later preserves point density distributions (if a segment of the point-cloud was represented with many points in close proximity, a larger number of those will remain after downsampling), voxel-based downsampling results in a regularised point distribution. This can result in a loss of local features (if voxels are too large for the required level of detail), but makes large-scale point-cloud representation more efficient."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "voxel_down_pcd = pcd.voxel_down_sample(voxel_size=2.5)\n",
    "o3d.visualization.draw_geometries([voxel_down_pcd], zoom=1, front=[0,0,1], lookat=[0, 0, 0], up=[0, 1, 0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.4 K-d Tree / Octree\n",
    "The [K-D tree (k-domensional tree)](https://en.wikipedia.org/wiki/K-d_tree) and the [Octree](https://en.wikipedia.org/wiki/Octree) are space-partitioning structures. For three-dimensional spaces, such as the point-clouds (used with Cartesian coordinates), the k-d tree can be understood as halving e.g. a cube by a plane along one coordinate axis, and continuing to halve the resulting substructures by iterating through the coordinate axes. An octree can be pictured similarly, as dividing our cube into eight evenly sized smaller cubes, and with each further step subdividing any \"parent\" structure into eight children.\n",
    "\n",
    "Without delving further into detail , the main benefit of both partitioning methods for point-cloud processing is to optimise analysis of point neighbourhoods, which are used to compute numerous point-cloud features, or apply filtering operations. Without a structuring element, identifying a point's nearest neighbours in a point-cloud would require computing all point-to-point distances, which would be very time-intensive. With a tree-structure, the nearest-neighbour search can be restricted to select subsets of the point-cloud.\n",
    "\n",
    "k-d trees and octrees can be computed in Open3D, as shown by the two following examples. For the purpose of this workshop, the more important element is noise filtering of a point-cloud, which internally makes use of a tree-structure for optimisation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pcd_tree = o3d.geometry.KDTreeFlann(pcd) # Computes a k-d tree\n",
    "\n",
    "octree = o3d.geometry.Octree(max_depth=6) # Initialises an empty octree structure with 6 layers\n",
    "octree.convert_from_point_cloud(pcd, size_expand=0.01) # divides the point-cloud into the octree cells\n",
    "o3d.visualization.draw_geometries([octree]) # for an octree, visualisation shows also the subdivision structure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.5 Outlier Removal\n",
    "As point-clouds can suffer from noise, Open3D provides two functions for removing unwanted artifacts from point clouds. These are radius-based and statistical outlier removal.\n",
    "\n",
    "The function `statistical_outlier_removal` determines points to be inliers or outliers based on their distance to neighbouring points compared to the cloud's average nearest-neighbour distance. It requires two arguments, `nb_neighbours` and `std_ratio`. The first argument defines how many neighbouring points are used for computing the average distances, whereas the second sets the outlier threshold. If this threshold is set lower, the function will filter more aggressively (removing more points as outliers).\n",
    "\n",
    "The function `radius_outlier_removal` instead filters by the amount of neighbours that a point has within a sperical volume around it. Its two parameters are `nb_points` and `radius`, where the first is the minimum of neighbours a point must have within the sphere to be counted as an inlier, and the second is the radius of said sphere.\n",
    "\n",
    "Both functions return two variables, the first of which (denoted `cloud` in the examples below) is the unmodified point-cloud, whereas the second (`ind`) is a vector containing the indexes of all inliers. This can be used to createseparate entities for the inliers respectively outliers.\n",
    "\n",
    "**Note:** The outlier removal uses the downsampled point-cloud as input."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pcd = o3d.io.read_point_cloud(\"../data/open3D-Data/CloudSample.pcd\").voxel_down_sample(voxel_size=2.5)\n",
    "cloud, ind = pcd.remove_radius_outlier(nb_points=10, radius=10)\n",
    "filtered_rad = pcd.select_by_index(ind) # This returns all inlier points\n",
    "outliers_rad = pcd.select_by_index(ind, invert=True) # By inverting the selection, the outliers are returned\n",
    "\n",
    "print(\"Showing outliers (red) and inliers (gray): \")\n",
    "outliers_rad.paint_uniform_color([1, 0, 0])\n",
    "filtered_rad.paint_uniform_color([0.8, 0.8, 0.8])\n",
    "o3d.visualization.draw_geometries([filtered_rad, outliers_rad])\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pcd = o3d.io.read_point_cloud(\"../data/open3D-Data/CloudSample.pcd\").voxel_down_sample(voxel_size=2.5)\n",
    "cloud, ind = pcd.remove_statistical_outlier(nb_neighbors=100, std_ratio=2.0)\n",
    "filtered_stat = pcd.select_by_index(ind) # This returns all inlier points\n",
    "outliers_stat = pcd.select_by_index(ind, invert=True) # By inverting the selection, the outliers are returned\n",
    "\n",
    "print(\"Showing outliers (red) and inliers (gray): \")\n",
    "outliers_stat.paint_uniform_color([1, 0, 0])\n",
    "filtered_stat.paint_uniform_color([0.8, 0.8, 0.8])\n",
    "o3d.visualization.draw_geometries([filtered_stat, outliers_stat])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.6 Hands-On Experience #1\n",
    "Now that all program elements have been introduced, it's time to combine them for the first stage in solving today's point-cloud problem: Preprocessing of point-cloud segments. The Python script below has been prepared for this purpose. The code itself is ready to execute, but there are some parameters that can be modified to influence the process results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Importing point-cloud files for the session\n",
    "# To apply preprocessing on both equally, this operates on the point-clouds before their orientation offset is applied\n",
    "pcd1 = o3d.io.read_point_cloud('../data/open3D-Data/Cloud_Segment_1.pcd')\n",
    "pcd2 = o3d.io.read_point_cloud('../data/open3D-Data/Cloud_Segment_2.pcd')\n",
    "\n",
    "# An axis indicator for the coordinate system\n",
    "aid = o3d.geometry.TriangleMesh.create_coordinate_frame(size= 20)\n",
    "\n",
    "## Some functions to use:\n",
    "\n",
    "# Function for Statistical Outlier Removal\n",
    "# param: pcd = o3d handle of point cloud entity\n",
    "# param: min_point = sample count for distance mean (SOR)\n",
    "# param: thresh = scale for distance standard deviation (SOR)\n",
    "def filter_SOR(pcd, min_point, thresh):\n",
    "    cl, ind = pcd.remove_statistical_outlier(nb_neighbors=min_point, std_ratio=thresh)\n",
    "    reduced = pcd.select_by_index(ind)\n",
    "    return reduced\n",
    "\n",
    "# Function for Radius Outlier Removal\n",
    "# param: pcd = o3d handle of point cloud entity\n",
    "# param: min_point = neighbour count threshold (ROR)\n",
    "# param: thresh = radius of neighbourhood (ROR)\n",
    "def filter_ROR(pcd, min_point, thresh):\n",
    "    cl, ind = pcd.remove_radius_outlier(nb_points=min_point, radius=thresh)\n",
    "    reduced = pcd.select_by_index(ind)\n",
    "    return reduced\n",
    "\n",
    "# Function to crop point-cloud by x-y-plane at defined cut-off (all points below are discarded)\n",
    "# param: pcd = o3d handle of point cloud entity\n",
    "# param: z_cutoff = floating-point height-cutoff\n",
    "# return: pcd_cropped = processed output\n",
    "def crop_pcd(pcd, z_cutoff):\n",
    "    pcd_array = np.asarray(pcd.points) # returns a numpy array of all points\n",
    "    new_pcd_array = [] # new numpy array\n",
    "    for point in pcd_array:\n",
    "        if z_cutoff < point[2]:\n",
    "            new_pcd_array.append(point) # retain every point where z-coordinate is above cutoff\n",
    "    pcd_cropped = o3d.geometry.PointCloud() # empty point-cloud\n",
    "    pcd_cropped.points = o3d.utility.Vector3dVector(new_pcd_array) # reconvert from numpy array to point-cloud\n",
    "    return pcd_cropped\n",
    "\n",
    "# Function to uniformly downsample the input\n",
    "# param: pcd = o3d handle of point cloud entity\n",
    "# param: threshold = sample rate (int, uniform)\n",
    "# return: pcd_reduced\n",
    "def reduce_uniform(pcd, threshold):\n",
    "    return pcd.uniform_down_sample(every_k_points=threshold)\n",
    "\n",
    "# Function to downsample the input by voxels\n",
    "# param: pcd = o3d handle of point cloud entity\n",
    "# param: threshold = voxel size (float, voxel sampling)\n",
    "# return: pcd_reduced\n",
    "def reduce_voxel(pcd, threshold):\n",
    "    return pcd.voxel_down_sample(voxel_size=threshold)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Instructions\n",
    "Five functions can be used for preprocessing (see source code above):\n",
    "- Downsampling (voxel-based and uniform downsampling)\n",
    "- Filtering (statistical and radius-based)\n",
    "- Cropping (Only along height / z-axis for the specific application sample)\n",
    "\n",
    "The functions can be called without the need to modify code by typing one of these keywords. Alongside the keyword itself, you need to provide parameters in the same line. Only press \"ENTER\" after your input is complete; the application only accepts single-line inputs.\n",
    "- Sample-Voxel [a]: [a] = voxel size (decimal number)\n",
    "- Sample-Uniform [a]: [a] = rate (every a-th point is taken) (integer number)\n",
    "- Filter-Radius [a] [b]: [a] = point count (integer number); [b] = radius (decimal number)\n",
    "- Filter-Statistical [a] [b]: [a] = point count (integer number); [b] = point-distance standard distribution (decimal number)\n",
    "- Crop [a]: [a] = minimal z-coordinate left in point-cloud (decimal number)\n",
    "\n",
    "In addition, the following utilities are available:\n",
    "- Save : results can be saved (this exits the program loop, and You will need to rerun the code cell below to restart)\n",
    "- View [a] : Show the current state of a point-cloud, specify [a] = 1 or 2 or 3 to choose. 3 means show both point clouds. pcd1 will be shown red. pcd2 will be shown blue\n",
    "\n",
    "**The Visualization always includes the origin of the common coordinate system.**\n",
    "\n",
    "The suggested parameters and sequence of execution are shown in this example:\n",
    "1. \"Crop -0.5\"\n",
    "2. \"Sample-Voxel 0.5\"\n",
    "3. \"Filter-Radius 10 2.0\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "while(1) :\n",
    "    print('1st Hands-On Session: Type your commands to process the point-clouds\\n')\n",
    "    line = input('Type your command: ')\n",
    "    \n",
    "    args = line.split()\n",
    "    key = args[0]\n",
    "    \n",
    "    if key == 'Filter-Radius' and len(args)>=3 :\n",
    "        points = int(args[1])\n",
    "        rad = float(args[2])\n",
    "        print('Applying radius-based filtering.')\n",
    "        pcd1 = filter_ROR(pcd1,points,rad)\n",
    "        pcd2 = filter_ROR(pcd2,points,rad)\n",
    "        \n",
    "        \n",
    "    elif key == 'Filter-Statistical' and len(args)>=3 :\n",
    "        points = int(args[1])\n",
    "        std_ratio = float(args[2])\n",
    "        print('Applying statistical filtering.')\n",
    "        pcd1 = filter_SOR(pcd1,points,std_ratio)\n",
    "        pcd2 = filter_SOR(pcd2,points,std_ratio)\n",
    "        \n",
    "    elif key == 'Sample-Voxel' and len(args)>=2 :\n",
    "        v_size = float(args[1])\n",
    "        print('Applying voxel downsampling.')\n",
    "        pcd1 = reduce_voxel(pcd1,v_size)\n",
    "        pcd2 = reduce_voxel(pcd2,v_size)\n",
    "        \n",
    "    elif key == 'Sample-Uniform' and len(args)>=2 :\n",
    "        points = int(args[1])\n",
    "        print('Applying uniform downsampling.')\n",
    "        pcd1 = reduce_uniform(pcd1,points)\n",
    "        pcd2 = reduce_uniform(pcd2,points)\n",
    "        \n",
    "    elif key == 'Crop' and len(args)>=2 :\n",
    "        cutoff = float(args[1])\n",
    "        print('Applying height cropping.')\n",
    "        pcd1 = crop_pcd(pcd1,cutoff)\n",
    "        pcd2 = crop_pcd(pcd2,cutoff)\n",
    "            \n",
    "    elif key == 'View' and len(args)>=2 :\n",
    "        sel = int(args[1])\n",
    "        pcd1.paint_uniform_color([1, 0, 0])\n",
    "        pcd2.paint_uniform_color([0, 0, 1])\n",
    "        if sel == 1:\n",
    "            o3d.visualization.draw_geometries([pcd1, aid])\n",
    "        elif sel == 2:\n",
    "            o3d.visualization.draw_geometries([pcd2, aid])\n",
    "        elif sel==3:\n",
    "            o3d.visualization.draw_geometries([pcd1, pcd2, aid])\n",
    "        else:\n",
    "            print('Invalid point-cloud index!\\n')\n",
    "                \n",
    "    elif key == 'Save' :\n",
    "        o3d.io.write_point_cloud('../data/o3D-Save/Cloud_Segment_1_prep.pcd', pcd1)\n",
    "        o3d.io.write_point_cloud('../data/o3D-Save/Cloud_Segment_2_prep.pcd', pcd2)\n",
    "        break # exit the loop\n",
    "        \n",
    "    else:\n",
    "        print('Error: Please retype command!\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "With the examples above, the following functions of Open3D were introduced:\n",
    "- Importing and exporting files\n",
    "- Displaying point-clouds\n",
    "- creating voxel grids and using them for point-cloud downsampling\n",
    "- tree-structures (k-d tree, octree) and their application\n",
    "- outlier removal functions\n",
    "\n",
    "Furthermore, the first hands-on session has produced preprocessed point-clouds serving as the basis for the next section of the workshop.\n",
    "\n",
    "The workshop continues with \"O3D-ManualAlignment.ipynb\"."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5 Registration refinement via ICP \n",
    "In the previous section of the workshop, manually adjusted parameters, along with visual inspection, were used to roughly align two point-cloud segments, using the partial overlap between both. The [Iterative Closest Point (ICP)](https://en.wikipedia.org/wiki/Iterative_closest_point) algorithm is a commonly used method for the task of minimising the remaining error.\n",
    "\n",
    "Its underlying principle is that from an initial alignment (**Note:** an alignment, however coarse, is indispensable for ICP) between a static _reference_ and a transformable _source_, for all (or at least a selection of) points in the source point-cloud, a closest corresponding point can be found in the reference point-cloud. A transformation is then estimated to reduce the offsets across this set of point-pairs. Following the trnasformation, another iteration is executed, consisting of associating closest points, estiamting and applying a suitable transformation.\n",
    "\n",
    "In Open3D, the corresponding function has an input parameter to define the correspondence threshold. The greater this value is chosen, the initial alignment is given greater tolerance for finding probable candiates for point correspondence pairs between source and target."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###### Import Required Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import open3d as o3d\n",
    "import numpy as np\n",
    "import copy\n",
    "import os\n",
    "import sys\n",
    "import time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5.1 ICP Implementation\n",
    "### Helper visualization function\n",
    "The function below visualizes a target point cloud and a source point cloud transformed with an alignment transformation. The target point cloud and the source point cloud are painted with cyan and yellow colors respectively. The more and tighter the two point clouds overlap with each other, the better the alignment result.\n",
    "\n",
    "**Note:** As with the code shown in the previous notebook, creating a true copy of the data is recommended, since functions like `paint_uniform_colour` or `transform` modify the point-cloud."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def draw_registration_result(source, target, transformation):\n",
    "    source_temp = copy.deepcopy(source)\n",
    "    target_temp = copy.deepcopy(target)\n",
    "    source_temp.paint_uniform_color([0.0, 0.0, 0.75])\n",
    "    target_temp.paint_uniform_color([0.75, 0.0, 0.0])\n",
    "    source_temp.transform(transformation)\n",
    "    o3d.visualization.draw_geometries([source_temp, target_temp])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Input\n",
    "The code below reads a source point cloud and a target point cloud from two files.\n",
    "\n",
    "    \n",
    "**Note:** The initial alignment is usually obtained by a global registration algorithm, or is achieved manually, as in this workshop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "target = o3d.io.read_point_cloud(\"../data/open3D-Data/ICPdemo_Segment_1.pcd\") # The reference point-cloud\n",
    "source = o3d.io.read_point_cloud(\"../data/open3D-Data/ICPdemo_Segment_2.pcd\") # The point-cloud to align to it\n",
    "\n",
    "threshold = 0.5 # Maximum distance to include points as correspondence pairs\n",
    "trans_init = np.eye(4) # The transformation matrix is initialized as a 4x4 identity matrix\n",
    "trans_init[:3, :3] = source.get_rotation_matrix_from_xyz((0, 0, 0)) # Rotation matrix is T[0-2, 0-2] (3x3)\n",
    "draw_registration_result(source, target, trans_init)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `evaluate_registration` calculates two main metrics:\n",
    "\n",
    "- `fitness`, which measures the overlapping area (# of inlier correspondences / # of points in target). The higher the better.\n",
    "- `inlier_rmse`, which measures the RMSE of all inlier correspondences. The lower the better."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Initial alignment\")\n",
    "evaluation = o3d.pipelines.registration.evaluate_registration(source, target, threshold, trans_init)\n",
    "print(evaluation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Point-to-point ICP\n",
    "In general, the ICP algorithm iterates over two steps:\n",
    "\n",
    "1. Find correspondence set $\\mathcal{K}=\\{(\\mathbf{p}, \\mathbf{q})\\}$ from target point cloud $\\mathbf{P}$, and source point cloud $\\mathbf{Q}$ transformed with current transformation matrix $\\mathbf{T}$.\n",
    "2. Update the transformation $\\mathbf{T}$ by minimizing an objective function $E(\\mathbf{T})$ defined over the correspondence set $\\mathcal{K}$.\n",
    "\n",
    "Different variants of ICP use different objective functions $E(\\mathbf{T})$ [\\[BeslAndMcKay1992\\]](../reference.html#beslandmckay1992) [\\[ChenAndMedioni1992\\]](../reference.html#chenandmedioni1992) [\\[Park2017\\]](../reference.html#park2017).\n",
    "\n",
    "We first show a point-to-point ICP algorithm [\\[BeslAndMcKay1992\\]](../reference.html#beslandmckay1992) using the objective\n",
    "\n",
    "\\begin{equation}\n",
    "E(\\mathbf{T}) = \\sum_{(\\mathbf{p},\\mathbf{q})\\in\\mathcal{K}}\\|\\mathbf{p} - \\mathbf{T}\\mathbf{q}\\|^{2}\n",
    "\\end{equation}\n",
    "\n",
    "The class `TransformationEstimationPointToPoint` provides functions to compute the residuals and Jacobian matrices of the point-to-point ICP objective. The function `registration_icp` takes it as a parameter and runs point-to-point ICP to obtain the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Apply point-to-point ICP\")\n",
    "\n",
    "t = time.time()\n",
    "\n",
    "reg_p2p = o3d.pipelines.registration.registration_icp(source, target, threshold, trans_init,\n",
    "                                                      o3d.pipelines.registration.TransformationEstimationPointToPoint())\n",
    "print('Elapsed: ', time.time() - t,'s')\n",
    "\n",
    "print(reg_p2p)\n",
    "print(\"Transformation is:\")\n",
    "print(reg_p2p.transformation)\n",
    "draw_registration_result(source, target, reg_p2p.transformation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default, `registration_icp` runs until convergence or reaches a maximum number of iterations (30 by default). It can be changed to allow more computation time and to improve the results further. In this case, the first 30 iterations have yielded an  improvement over the initial alignment, with an increase in `fitness` from 0.216374 to 0.321547, and a reduction of `inlier_rmse` from 0.249834 to 0.221251.\n",
    "\n",
    "Continuing the ICP calculations to a greater number of iterations (2000) yields the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Apply point-to-point ICP with greater iterations limit\")\n",
    "\n",
    "t = time.time()\n",
    "\n",
    "reg_p2p = o3d.pipelines.registration.registration_icp(\n",
    "    source, target, threshold, trans_init,\n",
    "    o3d.pipelines.registration.TransformationEstimationPointToPoint(),\n",
    "    o3d.pipelines.registration.ICPConvergenceCriteria(max_iteration=2000))\n",
    "\n",
    "print('Elapsed: ', time.time() - t,'s')\n",
    "\n",
    "print(reg_p2p)\n",
    "print(\"Transformation is:\")\n",
    "print(reg_p2p.transformation)\n",
    "draw_registration_result(source, target, reg_p2p.transformation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At 2000 iterations, calculation time is considerably longer. However, the final alignment is also significantly tighter. The `fitness` score improves to 0.473270. The `inlier_rmse` reduces to 0.015437."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Point-to-plane ICP\n",
    "The point-to-plane ICP algorithm [\\[ChenAndMedioni1992\\]](../reference.html#chenandmedioni1992) uses a different objective function\n",
    "\n",
    "\\begin{equation}\n",
    "E(\\mathbf{T}) = \\sum_{(\\mathbf{p},\\mathbf{q})\\in\\mathcal{K}}\\big((\\mathbf{p} - \\mathbf{T}\\mathbf{q})\\cdot\\mathbf{n}_{\\mathbf{p}}\\big)^{2},\n",
    "\\end{equation}\n",
    "\n",
    "where $\\mathbf{n}_{\\mathbf{p}}$ is the normal of point $\\mathbf{p}$. [\\[Rusinkiewicz2001\\]](../reference.html#rusinkiewicz2001) has shown that the point-to-plane ICP algorithm has a faster convergence speed than the point-to-point ICP algorithm.\n",
    "\n",
    "`registration_icp` is called with a different parameter `TransformationEstimationPointToPlane`. Internally, this class implements functions to compute the residuals and Jacobian matrices of the point-to-plane ICP objective.\n",
    "\n",
    "**Note:** Using a Point-to-Plane objective function requires to compute beforehand the normals for the points of the target point-cloud (the static element the _source_ is to be aligned to). This can be achieved with the `estimate_normals` function, using a k-d tree to speed up computation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Apply point-to-plane ICP\")\n",
    "\n",
    "t = time.time()\n",
    "\n",
    "target.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=5, max_nn=1000))\n",
    "reg_p2l = o3d.pipelines.registration.registration_icp(\n",
    "    source, target, threshold, trans_init,\n",
    "    o3d.pipelines.registration.TransformationEstimationPointToPlane())\n",
    "\n",
    "print('Elapsed: ', time.time() - t,'s')\n",
    "\n",
    "print(reg_p2l)\n",
    "print(\"Transformation is:\")\n",
    "print(reg_p2l.transformation)\n",
    "draw_registration_result(source, target, reg_p2l.transformation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The point-to-plane ICP within 30 iterations reaches tighter alignment than point-to-point ICP (a `fitness` score of 0.407211 and an `inlier_rmse` score of 0.181308). However, the need to compute normals first means the overall computation time is greater."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5.2 Hands-On Experience #3\n",
    "This session is not hands-on in the same extent as the prior ones. The purpose is to improve the result of your preceding manual alignment. The program prepared for You is using point-to-point ICP, and allows you to set two parameters: `threshold` (recommended to use a value between 0.05 and 0.5) and `iterations` (recommended to leave this value below 100 iterations).\n",
    "\n",
    "In the same manner as before, you can run and rerun the application by using one of the following keywords:\n",
    "- Run-ICP\n",
    "- Compare-Results : Computes perfromance relative to perfect alignment\n",
    "- Get-Progress : Computes improvement from current ICP result to initial (manual) alignment\n",
    "- View-ICP : Shows currently achieved alignment\n",
    "- View-Ref:  Shows template solution (perfect alignment)\n",
    "- Save : Writes the aligned point-clouds into a single file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Importing point-cloud segments to align\n",
    "target = o3d.io.read_point_cloud('../data/open3D-Data/Cloud_Segment_1_prep.pcd') # the static reference\n",
    "source = o3d.io.read_point_cloud('../data/o3D-Save/Cloud_Segment_2_aligned.pcd') # the point-cloud to align\n",
    "\n",
    "# As a reference, the files below show the original map (perfect alignment)\n",
    "target_ref = o3d.io.read_point_cloud('../data/open3D-Data/Vaart_MapSeg1.pcd')\n",
    "source_ref = o3d.io.read_point_cloud('../data/open3D-Data/Vaart_MapSeg2.pcd')\n",
    "\n",
    "# The initial trnasformation matrix only represents the orientation of source point-cloud relative to the coordinate system,\n",
    "# it does not introduce any further transformation\n",
    "trans_init = np.eye(4) # The transformation matrix is initialized as a 4x4 identity matrix\n",
    "trans_init[:3, :3] = source.get_rotation_matrix_from_xyz((0, 0, 0)) # Rotation matrix is T[0-2, 0-2] (3x3)\n",
    "\n",
    "# Run Point-to-Point ICP\n",
    "def run_ICP(threshold, iterations) :\n",
    "    t = time.time()\n",
    "\n",
    "    reg_p2p = o3d.pipelines.registration.registration_icp(\n",
    "        source, target, threshold, trans_init,\n",
    "        o3d.pipelines.registration.TransformationEstimationPointToPoint(),\n",
    "        o3d.pipelines.registration.ICPConvergenceCriteria(max_iteration=iterations))\n",
    "\n",
    "    print('Elapsed: ', time.time() - t,'s')\n",
    "    \n",
    "    return reg_p2p\n",
    "    \n",
    "# computes performance relative to template solution\n",
    "def ref_ICP(threshold, iterations) :\n",
    "    trans_ref = np.eye(4) # The transformation matrix is initialized as a 4x4 identity matrix\n",
    "    trans_ref[:3, :3] = source_ref.get_rotation_matrix_from_xyz((0, 0, 0)) # Rotation matrix is T[0-2, 0-2] (3x3)\n",
    "    return o3d.pipelines.registration.registration_icp(\n",
    "        source_ref, target_ref, threshold, trans_ref,\n",
    "        o3d.pipelines.registration.TransformationEstimationPointToPoint(),\n",
    "        o3d.pipelines.registration.ICPConvergenceCriteria(max_iteration=iterations))\n",
    "    \n",
    "def view_ICP(pcd1, pcd2, transformation) :\n",
    "    source_temp = copy.deepcopy(pcd2)\n",
    "    target_temp = copy.deepcopy(pcd1)\n",
    "    source_temp.transform(transformation)\n",
    "    o3d.visualization.draw_geometries([source_temp, target_temp])\n",
    "\n",
    "    \n",
    "#Before starting with ICP, check initial alignment\n",
    "init_eval = o3d.pipelines.registration.evaluate_registration(source, target, 0.5, trans_init)\n",
    "icp_eval = init_eval\n",
    "\n",
    "icp_ref = ref_ICP(0.01, 100)\n",
    "    \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "while(1) :\n",
    "    print('3rd Hands-On Session: Type your commands to run ICP to improve point-cloud alignment\\n')\n",
    "    line = input('Type your command: ')\n",
    "    \n",
    "    args = line.split()\n",
    "    key = args[0]\n",
    "    \n",
    "    if key == 'Run-ICP' and len(args)>=3 :\n",
    "        threshold = float(args[1])\n",
    "        iterations = int(args[2])\n",
    "        print('Computing Point-to-Point ICP.')\n",
    "        icp_eval = run_ICP(threshold,iterations)\n",
    "        view_ICP(target, source, icp_eval.transformation)\n",
    "        \n",
    "    elif key == 'Compare-Results' :\n",
    "        print('Fitness score achieved: ',icp_eval.fitness,'| Reference fitness score: ',icp_ref.fitness)\n",
    "        print('Inlier RMSE achieved: ',icp_eval.inlier_rmse,'| Reference RMSE: ',icp_ref.inlier_rmse)\n",
    "                    \n",
    "    elif key == 'View-ICP' :\n",
    "        view_ICP(target, source, icp_eval.transformation)\n",
    "    \n",
    "    elif key == 'View-Ref' :\n",
    "        view_ICP(target_ref, source_ref, icp_ref.transformation)    \n",
    "    \n",
    "    elif key == 'Get-Progress':\n",
    "        print('Fitness score changed from ',init_eval.fitness,' to ',icp_eval.fitness)\n",
    "        print('Inlier RMSE changed from ',init_eval.inlier_rmse,' to ',icp_eval.inlier_rmse)\n",
    "    \n",
    "    elif key == 'Save' :\n",
    "        output = source + target\n",
    "        output.paint_uniform_color([0.75, 0.0, 0.75]) # colour is mix of original solid colours\n",
    "        o3d.io.write_point_cloud('../data/o3D-Save/Merged_Clouds.pcd', output)\n",
    "        break # exit the loop\n",
    "        \n",
    "    else:\n",
    "        print('Error: Please retype command!\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "In this section, the ICP algorithm and its two implementations in Open3D were introduced. Automated point-cloud alignment using ICP was demonstrated on a sample dataset. In addition, the Hands-On session has produced a new point-cloud containing the aligned merging of two inputs.\n",
    "\n",
    "In the next and final section, the entire alignment process for point-clouds is implemented through hands-off functions of Open3D. Proceed with \"O3D-GlobalRegistration.ipynb\"."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "interpreter": {
   "hash": "f50e1d202e66ca9d1bb86231cb6414771e783b64b793893d086f00fc72b4d7af"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

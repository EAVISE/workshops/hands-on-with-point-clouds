{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6 Global registration\n",
    "The ICP algorithm is commonly called a local registration method, on account of requiring a prior (approximate) alignment between point-clouds. In order to employ ICP, such an initial alignment must be provided either manually, or through another algorithm.\n",
    "By contrast, methods referred to as **global registration** do not require an initial alignment. They usually rely on computing local features in the point-clouds to locate similar regions in other point-clouds. However, in contrast to local registration methods like ICP, they usually produce less tight alignment results and are used as initialization of the local methods.\n",
    "\n",
    "In this last section of the Open3D workshop, a global registration method is presented, and combined with ICP to deliver a hands-off alignment procedure for two point-clouds. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###### Import Required Libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Jupyter environment detected. Enabling Open3D WebVisualizer.\n",
      "[Open3D INFO] WebRTC GUI backend enabled.\n",
      "[Open3D INFO] WebRTCWindowSystem: HTTP handshake server disabled.\n"
     ]
    }
   ],
   "source": [
    "import open3d as o3d\n",
    "import numpy as np\n",
    "import copy\n",
    "import time\n",
    "import os\n",
    "import sys"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6.1 Visualization\n",
    "This helper function visualizes the transformed source point cloud together with the target point cloud:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def draw_registration_result(source, target, transformation):\n",
    "    source_temp = copy.deepcopy(source)\n",
    "    target_temp = copy.deepcopy(target)\n",
    "    source_temp.paint_uniform_color([0.0, 0.0, 0.75])\n",
    "    target_temp.paint_uniform_color([0.75, 0.0, 0.0])\n",
    "    source_temp.transform(transformation)\n",
    "    o3d.visualization.draw_geometries([source_temp, target_temp])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6.2 Extract geometric feature\n",
    "The first step towards global registration is computing local geometric features from _source_ (cloud to be aligned) and _target_ (static reference to align to) point-clouds. The method selected for this workshop is to compute a (Fast) Point-Feature Histogram (FPFH), which requires the normals on a local point neighbourhood. To reduce the computational load, the point-clouds are downsampled (depending on the preprocessing steps taken for noise removal, this addtional reduction might not be necessary). The FPFH feature is a 33-dimensional vector that describes the local geometric property of a point. A nearest neighbor query in the 33-dimensinal space can return points with similar local geometric structures.\n",
    "\n",
    "**Note:** Open3D's implementation of FPFH is based on [R. Rusu, N. Blodow, and M. Beetz (2009): Fast Point Feature Histograms (FPFH) for 3D registration](https://ieeexplore.ieee.org/document/5152473)\n",
    "\n",
    "The function below is used to implement the preprocessing and computation of FPFH:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def preprocess_point_cloud(pcd, voxel_size):\n",
    "    print(\":: Downsample with a voxel size %.3f.\" % voxel_size)\n",
    "    pcd_down = pcd.voxel_down_sample(voxel_size)\n",
    "\n",
    "    radius_normal = voxel_size * 2\n",
    "    print(\":: Estimate normal with search radius %.3f.\" % radius_normal)\n",
    "    pcd_down.estimate_normals(\n",
    "        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))\n",
    "\n",
    "    radius_feature = voxel_size * 5\n",
    "    print(\":: Compute FPFH feature with search radius %.3f.\" % radius_feature)\n",
    "    pcd_fpfh = o3d.pipelines.registration.compute_fpfh_feature(\n",
    "        pcd_down,\n",
    "        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100))\n",
    "    return pcd_down, pcd_fpfh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6.3 Input\n",
    "This code below reads a source point cloud and a target point cloud from two files. These sample files are the same ones that were used for the introduction to ICP in Open3D. They have been given a different (greater) misalignment; the transformation matrix therefore does not introduce any further offset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def prepare_dataset(voxel_size):\n",
    "    print(\":: Load two point clouds with disturbed initial pose.\")\n",
    "    target = o3d.io.read_point_cloud(\"../data/open3D-Data/GlobalRegdemo_Segment_1.pcd\") # The reference point-cloud\n",
    "    source = o3d.io.read_point_cloud(\"../data/open3D-Data/GlobalRegdemo_Segment_2.pcd\") # The point-cloud to align to it\n",
    "    trans_init = np.eye(4) # The transformation matrix is initialized as a 4x4 identity matrix\n",
    "    trans_init[:3, :3] = source.get_rotation_matrix_from_xyz((0, 0, 0)) # Rotation matrix is T[0-2, 0-2] (3x3)\n",
    "    \n",
    "    draw_registration_result(source, target, trans_init)\n",
    "\n",
    "    source_down, source_fpfh = preprocess_point_cloud(source, voxel_size)\n",
    "    target_down, target_fpfh = preprocess_point_cloud(target, voxel_size)\n",
    "    return source, target, source_down, target_down, source_fpfh, target_fpfh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      ":: Load two point clouds with disturbed initial pose.\n",
      ":: Downsample with a voxel size 1.500.\n",
      ":: Estimate normal with search radius 3.000.\n",
      ":: Compute FPFH feature with search radius 7.500.\n",
      ":: Downsample with a voxel size 1.500.\n",
      ":: Estimate normal with search radius 3.000.\n",
      ":: Compute FPFH feature with search radius 7.500.\n"
     ]
    }
   ],
   "source": [
    "voxel_size = 1.5  # means 1.5m voxel size for this dataset\n",
    "source, target, source_down, target_down, source_fpfh, target_fpfh = prepare_dataset(voxel_size)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6.4 RANSAC\n",
    "Using Point-Feature Histograms, a vector of local features is computed for each point. Global alignment is then achieved through pairing points between both point-clouds that have a sufficiently similar point-feature histogram. To find likely candidates, the RANSAC (**RAN**dom **SA**mple **C**onsensus) algorithm is used. In each iteration of RANSAC, a number of points are selected from the _source_ point-cloud, and their corresponding points in the _target_ cloud are located by querying the nearest neighbor in the 33-dimensional FPFH feature space. A pruning step takes fast pruning algorithms to quickly reject false matches early.\n",
    "\n",
    "Open3D provides the following pruning algorithms:\n",
    "- `CorrespondenceCheckerBasedOnDistance` checks if aligned point clouds are close (less than the specified threshold).\n",
    "- `CorrespondenceCheckerBasedOnEdgeLength` checks if the lengths of any two arbitrary edges (line formed by two vertices) individually drawn from source and target correspondences are similar. This tutorial checks that $||edge_{source}|| > 0.9 \\cdot ||edge_{target}||$ and $||edge_{target}|| > 0.9 \\cdot ||edge_{source}||$ are true.\n",
    "- `CorrespondenceCheckerBasedOnNormal` considers vertex normal affinity of any correspondences. It computes the dot product of two normal vectors. It takes a radian value for the threshold.\n",
    "\n",
    "Only matches that pass the pruning step are used to compute a transformation, which is validated on the entire point cloud. The core function is `registration_ransac_based_on_feature_matching`. The most important hyperparameter of this function is `RANSACConvergenceCriteria`. It defines the maximum number of RANSAC iterations and the confidence probability. The larger these two numbers are, the more accurate the result is, but also the more time the algorithm takes.\n",
    "\n",
    "As evidenced by the above, optimising parameters for RANSAC is not without its own complexities, but for the purposes of this workshop, these details will not be discussed further."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def execute_global_registration(source_down, target_down, source_fpfh,\n",
    "                                target_fpfh, voxel_size):\n",
    "    distance_threshold = voxel_size * 1.5\n",
    "    print(\":: RANSAC registration on downsampled point clouds.\")\n",
    "    print(\"   Since the downsampling voxel size is %.3f,\" % voxel_size)\n",
    "    print(\"   a liberal distance threshold %.3f is used.\" % distance_threshold)\n",
    "    result = o3d.pipelines.registration.registration_ransac_based_on_feature_matching(\n",
    "        source_down, target_down, source_fpfh, target_fpfh, True,\n",
    "        distance_threshold,\n",
    "        o3d.pipelines.registration.TransformationEstimationPointToPoint(False),\n",
    "        3, [\n",
    "            o3d.pipelines.registration.CorrespondenceCheckerBasedOnEdgeLength(\n",
    "                0.9),\n",
    "            o3d.pipelines.registration.CorrespondenceCheckerBasedOnDistance(\n",
    "                distance_threshold)\n",
    "        ], o3d.pipelines.registration.RANSACConvergenceCriteria(100000, 0.999))\n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      ":: RANSAC registration on downsampled point clouds.\n",
      "   Since the downsampling voxel size is 1.500,\n",
      "   a liberal distance threshold 2.250 is used.\n",
      "RegistrationResult with fitness=7.181512e-02, inlier_rmse=1.181404e+00, and correspondence_set size of 491\n",
      "Access transformation to get result.\n"
     ]
    }
   ],
   "source": [
    "result_ransac = execute_global_registration(source_down, target_down,\n",
    "                                            source_fpfh, target_fpfh,\n",
    "                                            voxel_size)\n",
    "print(result_ransac)\n",
    "draw_registration_result(source_down, target_down, result_ransac.transformation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6.5 Local refinement\n",
    "When comparing the above results (`fitness` and `inlier_rmse`) to the performance of ICP given in the previous notebook, it is evident that global registration has not yielded as tightly aligned point-clouds as ICP. Therefore, the results will be refined using Point-to-Point ICP as a local registration method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "def refine_registration(source, target, source_fpfh, target_fpfh, voxel_size):\n",
    "    distance_threshold = voxel_size * 0.4\n",
    "    print(\":: Point-to-Point ICP registration is applied on original point\")\n",
    "    print(\"   clouds to refine the alignment. This time a strict\")\n",
    "    print(\"   distance threshold %.3f is used.\" % distance_threshold)\n",
    "    result = o3d.pipelines.registration.registration_icp(source, target, distance_threshold, result_ransac.transformation,\n",
    "                                                      o3d.pipelines.registration.TransformationEstimationPointToPoint())\n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      ":: Point-to-Point ICP registration is applied on original point\n",
      "   clouds to refine the alignment. This time a strict\n",
      "   distance threshold 0.600 is used.\n",
      "RegistrationResult with fitness=4.735192e-01, inlier_rmse=1.996201e-02, and correspondence_set size of 2563838\n",
      "Access transformation to get result.\n"
     ]
    }
   ],
   "source": [
    "result_icp = refine_registration(source, target, source_fpfh, target_fpfh,\n",
    "                                 voxel_size)\n",
    "print(result_icp)\n",
    "draw_registration_result(source, target, result_icp.transformation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using ICP as a refining method, the resulting alignment improves to a `fitness` value of 0.473519, and `inlier_rmse` of 0.019962. Even though only the default 30 iterations are used for ICP computation, these results are comparable to the ICP demonstration using 2000 iterations, which had achieved finally a `fitness` score of 0.473270 and `inlier_rmse` of 0.015437.\n",
    "\n",
    "At the same time, the time-consuming process of manually aligning the point-clouds is not necessary when using this combined global-local registration approach."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6.6 Hands-On Experience #4\n",
    "The final Hands-On session of this workshop provides You with a small application to apply the same global-and-local alignment method to the dataset You preprocessed in Hands-On session 1. Compared to the earlier sessions, there is little to do here, but there is an interactive program available like in the prior sessions.\n",
    "\n",
    "The available functions are\n",
    "- Run [a]: performs global and local registration, using voxel size [a] (decimal number)\n",
    "- View : shows the achieved alignment result after running global-local registration\n",
    "- Save : Stores the result merged into one point-cloud"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4th Hands-On Session: Type your commands to perform global registration\n",
      "\n",
      "Type your command: Save\n"
     ]
    }
   ],
   "source": [
    "# Importing point-cloud segments to align\n",
    "target = o3d.io.read_point_cloud('../data/open3D-Data/Cloud_Segment_1_prep.pcd')\n",
    "source = o3d.io.read_point_cloud('../data/open3D-Data/Cloud_Segment_2_prep.pcd')\n",
    "\n",
    "# Show initial alignment\n",
    "trans_init = np.eye(4) # The transformation matrix is initialized as a 4x4 identity matrix\n",
    "trans_init[:3, :3] = source.get_rotation_matrix_from_xyz((0, 0, 0)) # Rotation matrix is T[0-2, 0-2] (3x3)\n",
    "draw_registration_result(source, target, trans_init)\n",
    "\n",
    "while(1) :\n",
    "    print('4th Hands-On Session: Type your commands to perform global registration\\n')\n",
    "    line = input('Type your command: ')\n",
    "    \n",
    "    args = line.split()\n",
    "    key = args[0]\n",
    "    \n",
    "    if key == 'Run' and len(args)>=2 :\n",
    "        voxel_size = float(args[1])\n",
    "        print('Calculating FPFH.')\n",
    "        source_down, source_fpfh = preprocess_point_cloud(source, voxel_size)\n",
    "        target_down, target_fpfh = preprocess_point_cloud(target, voxel_size)\n",
    "        \n",
    "        print('Calculating global registration.')\n",
    "        result_ransac = execute_global_registration(source_down, target_down, source_fpfh, target_fpfh, voxel_size)\n",
    "        print(result_ransac)\n",
    "        draw_registration_result(source_down, target_down, result_ransac.transformation)\n",
    "        \n",
    "        print('Calculating ICP refinement.')\n",
    "        result_icp = refine_registration(source, target, source_fpfh, target_fpfh, voxel_size)\n",
    "        print(result_icp)\n",
    "        draw_registration_result(source, target, result_icp.transformation)\n",
    "    \n",
    "    elif key == 'View' :\n",
    "        source_temp = copy.deepcopy(source)\n",
    "        source_temp.transform(result_icp.transformation)\n",
    "        o3d.visualization.draw_geometries([source_temp, target])\n",
    "    \n",
    "    elif key == 'Save' :\n",
    "        source_temp = copy.deepcopy(source)\n",
    "        source_temp.transform(result_icp.transformation)\n",
    "        output = target + source_temp\n",
    "        output.paint_uniform_color([0.75, 0.0, 0.75]) # colour is mix of original solid colours\n",
    "        o3d.io.write_point_cloud('../data/open3D-Data/GlobalReg_Clouds.pcd', output)\n",
    "        break # exit the loop\n",
    "        \n",
    "    else:\n",
    "        print('Error: Please retype command!\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "During this section, the general theory of global point-cloud registration is introduced using (Fast) Point-Feature Histograms and RANSAC. Furthermore, the performance of global registration is compared to local registration using ICP from a manually obtained initial alignment. Lastly, the alignment resulting from global registration is improved though ICP."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Workshop Summary\n",
    "In this workshop, the fundamentals of point-cloud processing with Open3D were presented. The more theoretical introduction was combined with a sequence of hands-on sessions accomplishing the task of aligning two point-cloud segments to each other. At first, an introduction to basic functions like file I/O, downsampling and noise removal is given, coupled with a hands-on session preprocessing the point-clouds for alignment. The second session introduced functions to shift and rotate point-clouds, and resulted in a manual approximate alignment of the two preprocessed cloud segments. Afterwards, the third section outlined the ICP algorithm, and used it to refine the manual alignment. The final section introduced global registration algorithms, and demonstrated their use to avoid the manual alignment task while achieving comparable point-cloud alignment."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "interpreter": {
   "hash": "f50e1d202e66ca9d1bb86231cb6414771e783b64b793893d086f00fc72b4d7af"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

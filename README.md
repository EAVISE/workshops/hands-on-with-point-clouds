# Hands on with Point Clouds

This repository contains the codes for hands on from the CUMULUS workshop on 3D Point Clouds. 

1st Workshop: 07 Oct, 2021
2nd Workshop: 15 Nov, 2021

## Contents
* Introduction to 3D point clouds with open3D
Point cloud is a collection of points that represents an object, a shape, or a scene (a large collection of points) in 3D. Every point is primarily characterized by its Cartesian Coordinates (X, Y, Z). It may contain secondary attributes (R, G, B, Intensity) etc. We aim to provide a hands on experience on 3D point clouds with [open3D](http://www.open3d.org/), a 3D data processing library.

* Advanced topics with 3D Point Clouds 
Point clouds are mostly used in Robotics, Autonomous Driving, Perception, and 3D scene understanding. We aim to provide some use cases presenting the advanced topics on 3D point clouds including machine learning. For the advanced topics, we recommend to use Nvidia GPU with [computing capability](https://gist.github.com/standaloneSA/99788f30466516dbcc00338b36ad5acf) = 6.1.

## Repository Structure
![img](.description/directory_structure.png)

## How to prepare the environment?
#### Step 1: Install [Anaconda](https://www.anaconda.com/).
(Anaconda is a package manager for scientific libraries and dependencies in Python programming language.)

#### Step 2: Verify anaconda installation.
* Windows 
  launch anaconda_prompt.
  confirm conda installation by running: ```conda list```
  
* Linux/Mac OS
  launch terminal.
  confirm conda installation by running: ```conda list```

#### Step 3: Create the conda environment
Run the following command to create the conda environment with path to environment.yml file.
```conda env create -f environment.yml```


### Docker
For the docker, use the dockerfile to create the docker image. It comes with everything preinstalled.
## Downloading Datasets
Download the required datasets from the following links. Unzip them and put them in data directory.
* [Data for 3D-Basics section](https://kuleuven.app.box.com/shared/static/km3mzk8fxgeb8lrp3sxve9iqao3nw00i/)

* [Data for Pose Estimation and Tracking](https://kuleuven.app.box.com/s/83zrp2kfbsb8k09j4rk0s0pp3l0lpijg)

* Data for Classification and Segmentation: [Modelnet40 dataset](https://modelnet.cs.princeton.edu/) and [S3DIS dataset](http://buildingparser.stanford.edu/dataset.html)


## Running the Code
* Navigate to the project directory (root directory= Hands-on-with-point-clouds) using cd in terminal/anaconda_prompt.
* Launch jupyter-notebook (terminal/anaconda prompt) by running:
  ```jupyter-notebook``` 
* It will launch the contents of the diretory in the default browser of your system. 
* Navigate to your desired notebook (.ipynb) in the browser to run it.


## Feedback
We enjoy receiving your feedbacks. You can always share your thoughts with us. You can also ask for further supports. Email us at,
patrick.vandewalle@kuleuven.be ,
eric.demeester@kuleuven.be ,
peter.slaets@kuleuven.be

## Issues & Bug Reports
We sincerely ask you to post bugs and issues related to the codes on GitLab only. We are swamped with day-to-day activities. If you see a bug in the code, PLEASE, file them on the issue tracker.


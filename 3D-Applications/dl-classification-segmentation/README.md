# Deep Learning on 3D Point Clouds for Object Classification and Semantic Segmentation
To access the repository for this section, follow the link:
https://github.com/fayjie92/PointNet-PointNet2

## Environment Setup
You can ignore this step from the [repository](https://github.com/fayjie92/PointNet) if you have already configured the environment with [environment.yml](../../environment.yml) (for the Basics) from the current repository.

If you want to only access the deep learning on 3D point clouds for Object classification and Semantic Segmentation, then you can follow the environment setup from the [repository](https://github.com/fayjie92/PointNet)


## How to run the code?
* Once the conda environment is setup (either cumulusworkshop or pointnet) you can launch the jupyter-notebook by typing the following:
```jupyter-notebook```

* Then navigate to the jupyter notebook you want to run in the browser that has launched the directory containing the codes.


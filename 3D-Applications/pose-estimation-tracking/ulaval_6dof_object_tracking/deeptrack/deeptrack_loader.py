import numpy as np
from ulaval_6dof_object_tracking.deeptrack.deeptrack_loader_base import DeepTrackLoaderBase


class DeepTrackLoader(DeepTrackLoaderBase):
    def __init__(self, root, pretransforms=[], posttransforms=[], target_transform=[], wrong_indices=[]):
        super(DeepTrackLoader, self).__init__(root, pretransforms, posttransforms, target_transform, wrong_indices)

    def from_index(self, index):
        rgbA, depthA, initial_pose = self.load_image(index)
        rgbB, depthB, transformed_pose = self.load_pair(index, 0)
        sample = [rgbA, depthA, rgbB, depthB, [initial_pose.to_parameters(), index]]
        pose_labels = transformed_pose.to_parameters().astype(np.float32)
        pose_labels[:3] /= float(self.metadata["translation_range"])
        pose_labels[3:] /= float(self.metadata["rotation_range"])
        if self.pretransforms:
            sample = self.pretransforms[0](sample)
        if self.posttransforms:
            data = sample[:-1]
            sample = self.posttransforms[0](data)
        return sample, [pose_labels]

    def get_raw_from_index(self, index):
        rgbA, depthA, initial_pose = self.load_image(index)
        rgbB, depthB, transformed_pose = self.load_pair(index, 0)
        # sample = [rgbA, depthA, rgbB, depthB, initial_pose.to_parameters()]
        # pose_labels = transformed_pose.to_parameters().astype(np.float32)
        sample = [rgbA, depthA, rgbB, depthB, initial_pose, transformed_pose]
        return sample
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import seaborn as sns

model_path = '/home/yanming/workspace/6dof-tracking/outputOCNR_b15'
#model_path = '/home/yanming/workspace/6dof-tracking/outputEvaluation/'
#sequence_score_path = '/home/yanming/workspace/test/output_l_0.001/clock_stability_near_1/scores/'
sequence_score_path = '/home/yanming/workspace/test/output_b128_org_augmentation_para/output_b128_org_augmentation_para/l_0.001/clock_occlusion_0_e12/scores_1/'
#model_csv = 'model_best.pth.tar_eval.csv'
model_csv = 'checkpoint12.pth.tar_eval.csv'

sample_loss_path = '/home/yanming/workspace/6dof-tracking/output/evaluatePn/debug/valid_sample_loss1.npy'
plot_save_path = '/home/yanming/workspace/6dof-tracking/outputClockNewRender200k_b15_b128/result/'

target_path = '/home/yanming/workspace/6dof-tracking/output/evaluatePn/debug/valid_target1.npy'

if not os.path.exists(plot_save_path):
    os.mkdir(plot_save_path)


plot_loss = False
plot_component_loss = False
plot_error = False
plot_sequence_loss = False
plot_sampleloss_distribution = True
plot_target_distribution = True
plot_error_target = True

target = np.load(target_path)
error = np.load(sample_loss_path)

get_bad_sample_list=True
import numpy as np
if get_bad_sample_list:
    component_error = error[:,:-1]
    component_error_max = np.amax(component_error, 1)
    wrong_index = np.where(component_error_max > 0.10)
    print('wrong_index:',wrong_index)
    np.save('/home/yanming/workspace/6dof-tracking/output/evaluatePn/debug/wrong_index.npy', wrong_index)

if plot_error_target:

    MSE = error[:,-1]
    xyz=target[:,0:3]*1000
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt

    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    #
    # sp = ax.scatter(xyz[:, 0], xyz[:, 1], xyz[:, 2], s=20, c=MSE)
    # plt.colorbar(sp)
    # plt.show()
    threshold = 0.02
    i, = np.where(MSE > threshold)
    MSE_big = MSE[i]
    target_big = xyz[i]

    i, = np.where(MSE <= threshold)
    MSE_small = MSE[i]
    target_small = xyz[i]

    mean_big=np.mean(target_big, axis=0)
    mean_small = np.mean(target_small, axis=0)
    print('mean big', mean_big)
    print('mean_small', mean_small)

    from numpy import linalg as LA
    target_big_norm = np.apply_along_axis(np.linalg.norm, 1, target_big)
    target_small_norm = np.apply_along_axis(np.linalg.norm, 1, target_small)

    print('target_big_norm',np.mean(target_big_norm))
    print('target_small_norm',np.mean(target_small_norm))

    fig, axis = plt.subplots()
    sns.distplot(target_big_norm)
    plt.title("target_big_norm")
    plt.show()

    fig, axis = plt.subplots()
    sns.distplot(target_small_norm)
    plt.title("target_small_norm")
    plt.show()


    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # sp = ax.scatter(target_big[:, 0], target_big[:, 1], target_big[:, 2], s=2, c=MSE_big)
    # plt.colorbar(sp)
    # plt.show()
    print('break')


train = np.genfromtxt(os.path.join(model_path, 'training_data.csv'), delimiter=',')
valid = np.genfromtxt(os.path.join(model_path, 'validation_data.csv'), delimiter=',')

def plot_train_valid_col(axis, train, valid, col_number, title_text):
    axis.plot(range(0,len(train)), train[:,col_number], label = 'training')
    axis.plot(range(0,len(valid)), valid[:,col_number], 'r', label = 'validation')
    axis.set_xlabel('epoch')
    axis.set_ylabel('MSE')
    axis.set_title(title_text)


if plot_loss:
    fig, axis = plt.subplots()
    plot_train_valid_col(axis, train, valid, 3, 'MSE')
    plt.legend()
    plt.savefig(os.path.join(plot_save_path, 'training_validation_loss_vs_epoch.png'))
    plt.show()


if plot_component_loss:
    fig, axis = plt.subplots(2, 3)
    ax1, ax2, ax3 = axis[0, :]
    ax4, ax5, ax6 = axis[1, :]
    plot_train_valid_col(ax1, train, valid, 4, 'tx')
    plot_train_valid_col(ax2, train, valid, 5, 'ty')
    plot_train_valid_col(ax3, train, valid, 6, 'tz')
    plot_train_valid_col(ax4, train, valid, 7, 'Rx')
    plot_train_valid_col(ax5, train, valid, 8, 'Ry')
    plot_train_valid_col(ax6, train, valid, 9, 'Rz')
    plt.legend()
    plt.savefig(os.path.join(plot_save_path, 'component_loss.png'))
    plt.show()


if plot_error:
    fig, axis = plt.subplots()
    plot_train_valid_col(axis, train, valid, 10, 'error_t')
    plt.legend()
    plt.savefig(os.path.join(plot_save_path, 'error_t_vs_epoch.png'))
    plt.show()


    fig, axis = plt.subplots()
    plot_train_valid_col(axis, train, valid, 11, 'error_r')
    plt.legend()
    plt.savefig(os.path.join(plot_save_path, 'error_r_vs_epoch.png'))
    plt.show()



if plot_sequence_loss:
    #sequence_data = np.genfromtxt(os.path.join(sequence_score_path, 'model_best.pth.tar_eval.csv'), delimiter=',')
    sequence_data = pd.read_csv(os.path.join(sequence_score_path, model_csv))
    MSE = sequence_data['MSE'].to_numpy()
    Rx = sequence_data['Rx'].to_numpy()
    Ry = sequence_data['Ry'].to_numpy()
    Rz = sequence_data['Rz'].to_numpy()
    Tx = sequence_data['Tx'].to_numpy()
    Ty = sequence_data['Ty'].to_numpy()
    Tz = sequence_data['Tz'].to_numpy()
    error_t = sequence_data['error_t'].to_numpy()
    error_r = sequence_data['error_r'].to_numpy()

    length = len(MSE)+1

    fig, axis = plt.subplots()
    axis.plot(range(1, length), MSE, 'm', label='MSE')
    axis.plot(range(1, length), Rx, 'r', label='Rx')
    axis.plot(range(1, length), Ry, 'g',label='Ry')
    axis.plot(range(1, length), Rz, 'k', label='Rz')
    axis.plot(range(1, length), Tx, 'y',label='Tx')
    axis.plot(range(1, length), Ty, 'c', label='Ty')
    axis.plot(range(1, length), Tz, 'b', label='Tz')
    plt.legend()
    plt.title('MSE loss')
    plt.savefig(os.path.join(sequence_score_path,'MSE_loss.png'))
    plt.show()

    fig, axis = plt.subplots()
    axis.plot(range(1, length), error_t)
    plt.title('error_t')
    plt.savefig(os.path.join(sequence_score_path, 'error_t.png'))
    plt.show()

    fig, axis = plt.subplots()
    axis.plot(range(1, length), error_r)
    plt.title('error_r')
    plt.savefig(os.path.join(sequence_score_path, 'error_r.png'))
    plt.show()

    print('MSE: ', np.mean(MSE))
    print('Tx: ', np.mean(Tx))
    print('Ty: ', np.mean(Ty))
    print('Tz: ',np.mean(Tz))
    print('Rx: ', np.mean(Rx))
    print('Ry: ', np.mean(Ry))
    print('Rz: ', np.mean(Rz))
    print('error_t: ', np.mean(error_t))
    print('error_r: ', np.mean(error_r))

if plot_sampleloss_distribution:
    sample_loss = np.load(sample_loss_path)
    MSE_loss = sample_loss[:,-1]

    fig, axis = plt.subplots()
    sns.distplot(MSE_loss)
    plt.title("MSE_loss")
    plt.show()

    loss_mean = np.mean(MSE_loss)
    loss_std = np.std(MSE_loss)

    print('loss_mean: ',loss_mean)
    print('loss_std', loss_std)

    print((0.002+0.002+0.099+0.008+0.009+0.002)/6)

if plot_target_distribution:
    target = np.load(target_path)
    Tx = target[:,0]
    Ty = target[:, 1]
    Tz = target[:, 2]
    Rx = target[:, 3]
    Ry = target[:, 4]
    Rz = target[:, 5]

    fig, axis = plt.subplots()
    sns.distplot(Tx)
    plt.title("Tx")
    plt.show()

    fig, axis = plt.subplots()
    sns.distplot(Ty)
    plt.title("Ty")
    plt.show()

    fig, axis = plt.subplots()
    sns.distplot(Tz)
    plt.title("Tz")
    plt.show()

    fig, axis = plt.subplots()
    sns.distplot(Rx)
    plt.title("Rx")
    plt.show()

    fig, axis = plt.subplots()
    sns.distplot(Ry)
    plt.title("Ry")
    plt.show()

    fig, axis = plt.subplots()
    sns.distplot(Rz)
    plt.title("Rz")
    plt.show()


    target_mean = np.mean(target)
    target_std = np.std(target)

    print('target_mean: ',target_mean)
    print('target_std', target_std)




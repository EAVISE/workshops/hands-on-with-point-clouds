from pytorch_toolbox.io import yaml_load

from ulaval_6dof_object_tracking.utils.transform import Transform
from ulaval_6dof_object_tracking.utils.camera import Camera
from ulaval_6dof_object_tracking.deeptrack.deeptrack_loader_base import DeepTrackLoaderBase
from ulaval_6dof_object_tracking.utils.data import combine_view_transform, combine_view_transform_, show_frames, \
    compute_2Dboundingbox, normalize_scale, compute_axis, compute_box
from ulaval_6dof_object_tracking.utils.model_renderer import ModelRenderer
from ulaval_6dof_object_tracking.utils.plyparser import PlyParser
from ulaval_6dof_object_tracking.utils.pointcloud import maximum_width
from ulaval_6dof_object_tracking.utils.uniform_sphere_sampler import UniformSphereSampler
import cv2
from test_sequence import draw_debug
from tqdm import tqdm
import argparse
import shutil
import os
import math
import numpy as np

ESCAPE_KEY = 1048603


def draw_axis(img, pose, gt_pose, camera):
    axis = compute_axis(pose, camera)
    axis_gt = compute_axis(gt_pose, camera)

    cv2.line(img, tuple(axis_gt[0, ::-1]), tuple(axis_gt[1, ::-1]), (0, 0, 155), 3)
    cv2.line(img, tuple(axis_gt[0, ::-1]), tuple(axis_gt[2, ::-1]), (0, 155, 0), 3)
    cv2.line(img, tuple(axis_gt[0, ::-1]), tuple(axis_gt[3, ::-1]), (155, 0, 0), 3)

    cv2.line(img, tuple(axis[0, ::-1]), tuple(axis[1, ::-1]), (0, 0, 255), 3)
    cv2.line(img, tuple(axis[0, ::-1]), tuple(axis[2, ::-1]), (0, 255, 0), 3)
    cv2.line(img, tuple(axis[0, ::-1]), tuple(axis[3, ::-1]), (255, 0, 0), 3)


def draw_bbox(img, pose, width, camera):
    axis = compute_box(pose, camera, width)

    cv2.line(img, tuple(axis[0, ::-1]), tuple(axis[1, ::-1]), (0, 0, 255), 3)
    cv2.line(img, tuple(axis[1, ::-1]), tuple(axis[2, ::-1]), (0, 255, 0), 3)
    cv2.line(img, tuple(axis[2, ::-1]), tuple(axis[3, ::-1]), (255, 0, 0), 3)
    cv2.line(img, tuple(axis[3, ::-1]), tuple(axis[0, ::-1]), (255, 0, 0), 3)


def draw_bbox(img, axis):
    cv2.line(img, tuple(axis[0, ::-1]), tuple(axis[1, ::-1]), (0, 0, 255), 3)
    cv2.line(img, tuple(axis[1, ::-1]), tuple(axis[3, ::-1]), (0, 255, 0), 3)
    cv2.line(img, tuple(axis[3, ::-1]), tuple(axis[2, ::-1]), (255, 0, 0), 3)
    cv2.line(img, tuple(axis[2, ::-1]), tuple(axis[0, ::-1]), (255, 0, 0), 3)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Generate synthetic data for DeepTrack')

    parser.add_argument('-c', '--camera', help="camera json path", action="store",
                        default="../utils/camera_configs/camera-raw-training.json")  # camera.json
    parser.add_argument('--shader', help="shader path", action="store", default="../utils/shader")
    parser.add_argument('-o', '--output', help="save path", action="store", default="./generated_data")
    parser.add_argument('-m', '--model', help="model file path", action="store", default="./model_config.yaml")

    parser.add_argument('-y', '--saveformat', help="save format : png, numpy", action="store", default="numpy")

    parser.add_argument('-s', '--samples', help="quantity of samples", action="store", default=200000, type=int)
    parser.add_argument('-t', '--translation', help="max translation in meter", action="store", default=0.02,
                        type=float)
    parser.add_argument('-r', '--rotation', help="max rotation in degree", action="store", default=20, type=float)
    parser.add_argument('-e', '--resolution', help="image pixel size", action="store", default=150, type=int)
    parser.add_argument('--boundingbox', help="Crop bounding box width in % of the object max width",
                        action="store", default=10, type=int)

    parser.add_argument('-a', '--maxradius', help="max render distance", action="store", default=1.4, type=float)
    parser.add_argument('-i', '--minradius', help="min render distance", action="store", default=0.8, type=float)

    parser.add_argument('-v', '--show', help="show image while generating", action="store_true")
    parser.add_argument('-d', '--debug', help="show debug screen while generating", action="store_true")
    parser.add_argument('--crop', '--crop', help="render then crop", action="store_true")
    parser.add_argument('--random_sampling', help="sample displacement vector uniformly."
                                                  " Activating this will set the sampling"
                                                  " like Deep 6DOF Tracking paper.", action="store_true")

    arguments = parser.parse_args()

    TRANSLATION_RANGE = arguments.translation
    ROTATION_RANGE = math.radians(arguments.rotation)
    SAMPLE_QUANTITY = arguments.samples
    SPHERE_MIN_RADIUS = arguments.minradius
    SPHERE_MAX_RADIUS = arguments.maxradius
    IMAGE_SIZE = (arguments.resolution, arguments.resolution)
    SAVE_TYPE = arguments.saveformat
    SHOW = arguments.show
    DEBUG = arguments.debug
    RANDOM_SAMPLING = arguments.random_sampling

    SHADER_PATH = arguments.shader
    OUTPUT_PATH = arguments.output
    CAMERA_PATH = arguments.camera
    BOUNDING_BOX = arguments.boundingbox

    render_then_crop = arguments.crop

    ####################################################
    # x = np.array(1e5,1)
    # x = np.array([1e5, 1])
    # for i in range(0,1e5):
    #   sphere_sampler = UniformSphereSampler(SPHERE_MIN_RADIUS, SPHERE_MAX_RADIUS)
    #   random_transform = sphere_sampler.random_normal_magnitude(TRANSLATION_RANGE, ROTATION_RANGE)
    ################################
    MODELS = yaml_load(arguments.model)["models"]

    if SHOW:
        import cv2
    if not os.path.exists(OUTPUT_PATH):
        os.mkdir(OUTPUT_PATH)
    shutil.copy(arguments.model, os.path.join(OUTPUT_PATH, "models.yml"))

    # Write important misc data to file
    metadata = {}
    metadata["translation_range"] = str(TRANSLATION_RANGE)
    metadata["rotation_range"] = str(ROTATION_RANGE)
    metadata["image_size"] = str(IMAGE_SIZE[0])
    metadata["save_type"] = SAVE_TYPE
    metadata["object_width"] = {}
    metadata["min_radius"] = str(SPHERE_MIN_RADIUS)
    metadata["max_radius"] = str(SPHERE_MAX_RADIUS)

    camera = Camera.load_from_json(CAMERA_PATH)
    dataset = DeepTrackLoaderBase(OUTPUT_PATH, read_data=False)
    dataset.set_save_type(SAVE_TYPE)
    dataset.camera = camera
    window_size = (camera.width, camera.height)
    sphere_sampler = UniformSphereSampler(SPHERE_MIN_RADIUS, SPHERE_MAX_RADIUS)

    # If more than one model, the average bounding box is used.
    print("Compute Mean bounding box")
    widths = []
    for model in MODELS:
        geometry_path = os.path.join(model["path"], "geometry.ply")
        model_3d = PlyParser(geometry_path).get_vertex()
        object_max_width = maximum_width(model_3d) * 1000
        with_add = BOUNDING_BOX / 100 * object_max_width
        widths.append(int(object_max_width + with_add))
    widths.sort()
    OBJECT_WIDTH = widths[int(len(widths) / 2)]
    metadata["bounding_box_width"] = str(OBJECT_WIDTH)
    print("Mean object width : {}".format(OBJECT_WIDTH))

    real_dataset = DeepTrackLoaderBase("/home/yanming/workspace/6dof-tracking/data/data-r/valid")

    # Iterate over all models from config files
    for model in MODELS:
        geometry_path = os.path.join(model["path"], "geometry.ply")
        ao_path = os.path.join(model["path"], "ao.ply")
        vpRender = ModelRenderer(geometry_path, SHADER_PATH, dataset.camera, [window_size, IMAGE_SIZE])
        if os.path.exists(ao_path):
            vpRender.load_ambiant_occlusion_map(ao_path)
        # random_pose_para_all=np.empty((1,6))
        # for i in tqdm(range(SAMPLE_QUANTITY)):
        #     random_pose = sphere_sampler.get_random(False)
        #     random_pose_para = Transform.to_parameters(random_pose, isDegree=True)
        #     random_pose_para_all=np.vstack((random_pose_para_all, np.reshape(random_pose_para,(1,6))))
        #
        # print('plot random_pose_para')

        for i in tqdm(range(SAMPLE_QUANTITY)):
            frame, random_pose= real_dataset.data_pose[i]
            rgb_loadB, depth_loadB, random_transform = real_dataset.load_pair(i,0)
            # random_pose = sphere_sampler.get_random(False)
            # random_pose_para = Transform.to_parameters(random_pose, isDegree=True)
            #random_pose = Transform.from_parameters(-0.2, 0.2, -1.0, np.pi / 2, 0, 0)
            #random_pose = Transform.from_parameters(-0.007982954382896423, 0.1931561678647995,-0.7507527470588684, -0.8325386271797279, 0.7797724278421194, -0.7650716385594641)
            #random_pose = Transform.from_parameters(-0.041411206,0.22295424,-0.7449697,-0.7862021,0.5189557,-0.83481735, is_rodrigues=False)

            # random_pose = Transform.from_parameters(-0.023412298, 0.22359091, -0.7479859, -0.4584816, 0.28715962,
            #                                         -0.4644906, is_rodrigues=False)
            # random_pose = Transform.from_parameters(0., 0., -1.0, np.pi / 2, 0, 0)
            # random_pose = Transform.from_matrix(np.load('previous_pose_full_0_test.npy'))

            # if RANDOM_SAMPLING:
            #     # Sampling from uniform distribution in the ranges
            #     random_transform = Transform.random((-TRANSLATION_RANGE, TRANSLATION_RANGE),
            #                                         (-ROTATION_RANGE, ROTATION_RANGE))
            # else:
            #     # Sampling from gaussian ditribution in the magnitudes
            #     random_transform = sphere_sampler.random_normal_magnitude(TRANSLATION_RANGE, ROTATION_RANGE)
            #     #random_transform=Transform.from_matrix(np.load('pose_diff.npy'))
            #     #random_transform = Transform.from_parameters(0.05*TRANSLATION_RANGE,0.012*TRANSLATION_RANGE, 0.040*TRANSLATION_RANGE, 0.066*ROTATION_RANGE, 0.107*ROTATION_RANGE, 0.011*ROTATION_RANGE)
            #     #random_transform = Transform.from_parameters(0, 0, 0, 0, 0, 0)
            # random_pose = Transform.from_parameters(0.9,13.0,-1,-np.pi/2.0,0.15,0.8)
            # random_transform = Transform.from_parameters(0.02, 0.02, 0.02, np.pi*2.0,0.23, 0.5)
            #random_transform = Transform.from_parameters(-0.01937846839427948, 0.011359300464391708, -0.0013792639365419745, 0.16116105138053113, 0.05353056225494909, 0.15040924421713997)

            #random_transform = Transform.from_parameters(-0.0039491244,-0.019075438,-0.0041460926,0.12919691,-0.009957027,-0.110726915, is_rodrigues=True)
            pair = combine_view_transform(random_pose, random_transform)
            bb = compute_2Dboundingbox(random_pose, dataset.camera, OBJECT_WIDTH, scale=(1000, -1000, -1000))

            pair_para = Transform.to_parameters(pair)
            random_pose_para = Transform.to_parameters(random_pose)

            diff_para = random_pose_para - pair_para

            if render_then_crop:
                vpRender.setup_camera(camera, 0, camera.width, camera.height, 0)
                render_rgb, render_depth = vpRender.render_image(random_pose, fbo_index=0)
                rgbA, depthA = normalize_scale(render_rgb, render_depth, bb, IMAGE_SIZE)
            else:
                vpRender.setup_camera(camera, 0, camera.width, camera.height, 0)
                render_rgb, render_depth = vpRender.render_image(random_pose, fbo_index=0)
                draw_axis(render_rgb, random_pose, random_pose, camera)
                #draw_bbox(render_rgb, random_pose, OBJECT_WIDTH * 0.001, camera)
                draw_bbox(render_rgb, bb)
                show_frames(render_rgb, render_depth, render_rgb, render_depth)
                rgbA_rc, depthA_rc = normalize_scale(render_rgb, render_depth, bb, IMAGE_SIZE)

                bb_p = compute_2Dboundingbox(random_pose, dataset.camera, OBJECT_WIDTH, scale=(1000, 1000, -1000))

                left = np.min(bb_p[:, 1])
                right = np.max(bb_p[:, 1])
                top = np.min(bb_p[:, 0])
                bottom = np.max(bb_p[:, 0])
                vpRender.setup_camera(camera, left, right, bottom, top)
                rgbA, depthA = vpRender.render_image(random_pose, fbo_index=1)
                import cv2

                cv2.imwrite('rgbA_rc-camera.png', rgbA)
                cv2.imwrite('depthA_rc-camera.png', depthA)

                # draw_axis(rgbA, random_pose, random_pose, camera)

                #show_frames(rgbA_rc, depthA_rc, rgbA, depthA)

            # diff_RGB = rgbA-rgbA_tmp
            # diff_depth = depthA - depthA_tmp
            #
            # show_frames(rgbA, depthA, rgbA_tmp, depthA_tmp)
            # show_frames(diff_RGB, diff_depth, diff_RGB, diff_depth)
            # import cv2
            # cv2.imwrite(os.path.join(OUTPUT_PATH, 'A-{}.png').format(i), rgbA[:, :, ::-1])
            # cv2.imwrite(os.path.join(OUTPUT_PATH, 'A-{}d.png').format(i), depthA.astype(np.uint16))

            light_intensity = np.zeros(3)
            light_intensity.fill(np.random.uniform(0.1, 1.3))
            light_intensity += np.random.uniform(-0.1, 0.1, 3)
            ambiant_light = np.zeros(3)
            ambiant_light.fill(np.random.uniform(0.5, 1.5))
            vpRender.setup_camera(camera, 0, camera.width, camera.height, 0)
            rgbB, depthB = vpRender.render_image(pair,
                                                 fbo_index=0,
                                                 light_direction=sphere_sampler.random_direction(),
                                                 light_diffuse=light_intensity,
                                                 ambiant_light=ambiant_light)
            rgbB, depthB = normalize_scale(rgbB, depthB, bb, IMAGE_SIZE)
            #show_frames(rgbA, depthA, rgbB, depthB)
            show_frames(rgb_loadB, depth_loadB, rgbB, depthB)
            #
            # # cv2.imwrite(os.path.join(OUTPUT_PATH, 'before-crop-{}.png').format(i), rgbB[:, :, ::-1])
            # # cv2.imwrite(os.path.join(OUTPUT_PATH, 'before-crop-{}d.png').format(i), depthB.astype(np.uint16))
            #
            #
            #
            # index = dataset.add_pose(rgbA, depthA, random_pose)
            # dataset.add_pair(rgbB, depthB, random_transform, index)
            #
            # if i % 500 == 0:
            #     dataset.dump_images_on_disk()
            # if i % 5000 == 0:
            #     dataset.save_json_files(metadata)
            #
            # if DEBUG:
            #     show_frames(rgbA, depthA, rgbB, depthB)
            # if SHOW:
            #     cv2.imshow("test", np.concatenate((rgbA[:, :, ::-1], rgbB[:, :, ::-1]), axis=1))
            #     k = cv2.waitKey(1)
            #     if k == ESCAPE_KEY:
            #         break
    dataset.dump_images_on_disk()
    dataset.save_json_files(metadata)

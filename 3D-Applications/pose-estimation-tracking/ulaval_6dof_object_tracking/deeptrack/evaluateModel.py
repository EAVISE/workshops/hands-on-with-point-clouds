import sys
import os
from multiprocessing import cpu_count
import argparse

from pytorch_toolbox.io import yaml_load
from pytorch_toolbox.train_loop import TrainLoop
import torch
from torch import optim
from torch.utils import data
import numpy as np

from pytorch_toolbox.io import yaml_dump
from pytorch_toolbox.transformations.compose import Compose

from ulaval_6dof_object_tracking.deeptrack.data_augmentation import Occluder, HSVNoise, Background, GaussianNoise, \
    GaussianBlur, DepthDownsample, OffsetDepth, NormalizeChannels, ToTensor, ChannelHide, PadOccluderMask, SpecificBackground
from ulaval_6dof_object_tracking.deeptrack.deeptrack_callback import DeepTrackCallback
from ulaval_6dof_object_tracking.deeptrack.deeptrack_loader import DeepTrackLoader
from ulaval_6dof_object_tracking.deeptrack.deeptrack_net import DeepTrackNet
import datetime

if __name__ == '__main__':
    #
    #   load configurations from
    #

    parser = argparse.ArgumentParser(description='Train DeepTrack')
    parser.add_argument('-o', '--output', help="Output path", metavar="FILE")
    parser.add_argument('-d', '--dataset', help="Dataset path", metavar="FILE")
    parser.add_argument('-b', '--background', help="Background path", metavar="FILE")
    parser.add_argument('-r', '--occluder', help="Occluder path", metavar="FILE")
    parser.add_argument('-f', '--finetune', help="finetune path", default="None")
    parser.add_argument('-c', '--from_last', help="Continue training from last checkpoint", action="store_true")

    parser.add_argument('-i', '--device', help="Gpu id", action="store", default=0, type=int)
    parser.add_argument('-w', '--weightdecay', help="weight decay", action="store", default=0.000001, type=float)
    parser.add_argument('-l', '--learningrate', help="learning rate", action="store", default=0.001, type=float)
    parser.add_argument('-k', '--backend', help="backend : cuda | cpu", action="store", default="cuda")
    parser.add_argument('-e', '--epoch', help="number of epoch", action="store", default=25, type=int)
    parser.add_argument('-s', '--batchsize', help="Size of minibatch", action="store", default=128, type=int)
    parser.add_argument('-m', '--sharememory', help="Activate share memory", action="store_true")
    parser.add_argument('-n', '--ncore', help="number of cpu core to use, -1 is all core", action="store", default=-1, type=int)
    parser.add_argument('--nmodel', help="number of pretriend models", action="store", default=1,
                        type=int)
    parser.add_argument('-g', '--gradientclip', help="Activate gradient clip", action="store_true")
    parser.add_argument('--tensorboard', help="Size of minibatch", action="store_true")
    parser.add_argument('--model', help="3D model", metavar="FILE")
    parser.add_argument('--pretrained_model', help="pretrained_model", metavar="FILE")
    parser.add_argument('--save_sample_loss', help="log_sample_loss", action="store_true")
    parser.add_argument('--use_wrong_index', help="use_wrong_index", action="store_true")


    arguments = parser.parse_args()

    learning_rate = arguments.learningrate
    weight_decay = arguments.weightdecay
    device_id = arguments.device
    backend = arguments.backend
    epochs = arguments.epoch
    batch_size = arguments.batchsize
    use_shared_memory = arguments.sharememory
    number_of_core = arguments.ncore
    gradient_clip = arguments.gradientclip
    start_from_last = arguments.from_last
    use_tensorboard = arguments.tensorboard
    use_wrong_index = arguments.use_wrong_index

    output_path = arguments.output
    occluder_path = arguments.occluder
    background_path = arguments.background
    data_path = arguments.dataset
    finetune_path = arguments.finetune
    model_file = os.path.join(yaml_load(arguments.model)["models"][0]["path"], 'geometry.ply')
    pretrained_model=arguments.pretrained_model
    pretrained_path = os.path.dirname(arguments.pretrained_model)
    model_number=arguments.nmodel
    #pretrained_model = os.path.join(pretrained_path, 'model_best.pth.tar') #checkpoint6.pth.tar

    #
    #   Load configurations from file
    #

    data_path = os.path.expandvars(data_path)
    output_path = os.path.expandvars(output_path)
    occluder_path = os.path.expandvars(occluder_path)
    background_path = os.path.expandvars(background_path)
    finetune_path = os.path.expandvars(finetune_path)

    if not os.path.exists(output_path):
        os.mkdir(output_path)
    if number_of_core == -1:
        number_of_core = cpu_count()
    if backend == "cuda":
        torch.cuda.set_device(device_id)
    tensorboard_path = ""
    if use_tensorboard:
        tensorboard_path = os.path.join(output_path, "tensorboard_logs")

    #
    #   Instantiate models/loaders/etc.
    #
    loader_param = {}

    model_class = DeepTrackNet
    callbacks = DeepTrackCallback(output_path, log_sample_loss=arguments.save_sample_loss, is_dof_only=True)
    loader_class = DeepTrackLoader


    # Here we use the following transformations:
    images_mean = np.load(os.path.join(pretrained_path, "mean.npy"))
    images_std = np.load(os.path.join(pretrained_path, "std.npy"))
    # transfformations are a series of transform to pass to the input data. Here we have to build a list of
    # transforms for each inputs to the network's forward call

    pretransforms = [Compose([Occluder(occluder_path, 0.75)])]

    posttransforms = [Compose([HSVNoise(0.07, 0.05, 0.1),
                               Background(background_path),
                               GaussianNoise(2, 5),
                               GaussianBlur(6),
                               DepthDownsample(0.7),
                               ChannelHide(disable_proba=0.3),
                               OffsetDepth(),
                               NormalizeChannels(images_mean, images_std),
                               ToTensor()])]

    testtransforms_pre = [Compose([PadOccluderMask()])]
    testtransforms_post = [Compose([SpecificBackground(background_path), #Background(background_path),
                                    OffsetDepth(),
                               NormalizeChannels(images_mean, images_std),
                               ToTensor()])]

    print("Load datasets from {}".format(data_path))


    train_dataset = loader_class(os.path.join(data_path, "train"), pretransforms, posttransforms, **loader_param)

    if use_wrong_index:
        wrong_indices = np.load(os.path.join(data_path, 'wrong_index.npy'))
        valid_dataset = loader_class(os.path.join(data_path, "valid"), testtransforms_pre, testtransforms_post, wrong_indices = list(list(wrong_indices)[0]))
    else:
        valid_dataset = loader_class(os.path.join(data_path, "valid"), testtransforms_pre, testtransforms_post)
    # valid_dataset = loader_class(os.path.join(data_path, "valid"), testtransforms_pre,
    #                             testtransforms_post, wrong_indices = [1,0,2,6,5])
    test_dataset = loader_class(os.path.join(data_path, "test"), testtransforms_pre, testtransforms_post, **loader_param)

    # Save important information to output:
    print("Save meta data in {}".format(output_path))
    np.save(os.path.join(output_path, "mean.npy"), images_mean)
    np.save(os.path.join(output_path, "std.npy"), images_std)
    yaml_dump(os.path.join(output_path, "meta.yml"), train_dataset.metadata)

    # Instantiate the data loader needed for the train loop. These use dataset object to build random minibatch
    # on multiple cpu core
    train_loader = data.DataLoader(train_dataset,
                                   batch_size=batch_size,
                                   shuffle=True,
                                   num_workers=number_of_core,
                                   pin_memory=use_shared_memory,
                                   drop_last=True,
                                   )

    val_loader = data.DataLoader(valid_dataset,
                                 batch_size=batch_size,
                                 num_workers=number_of_core,
                                 pin_memory=use_shared_memory,
                                 )

    # Setup model
    model = model_class(image_size=int(train_dataset.metadata["image_size"]))
    if finetune_path != "None":
        finetune_path = os.path.expandvars(finetune_path)
        print("Finetuning path : {}".format(finetune_path))
        checkpoint = torch.load(finetune_path, map_location=lambda storage, loc: storage)
        model.load_state_dict(checkpoint['state_dict'])

    optimizer = optim.Adam(model.parameters(), lr=learning_rate, weight_decay=weight_decay)

    if use_tensorboard:
        from pytorch_toolbox.visualization.tensorboard_logger import TensorboardLogger
        date_str = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        tensorboard_logger = TensorboardLogger(os.path.join(tensorboard_path, date_str))

    for i in range(0,model_number):
        # setup pretrained model
        if model_number>1:
            pretrained_model=os.path.join(pretrained_path,"checkpoint"+str(i)+".pth.tar")
        tracker_model = DeepTrackNet(image_size=174)
        checkpoint = torch.load(pretrained_model)
        tracker_model.load_state_dict(checkpoint['state_dict'])
        epoch = checkpoint['epoch']
        tracker_model.eval()
        model = tracker_model

        # Instantiate the train loop and train the model.
        train_loop_handler = TrainLoop(model, train_loader, val_loader, test_dataset, optimizer, backend, gradient_clip,
                                       use_tensorboard=False, tensorboard_log_path=tensorboard_path)
        train_loop_handler.add_callback(callbacks)
        train_loop_handler.setup_renderer(train_dataset.metadata, model_file)#, '../utils/camera_configs/camera.json')

        loss,error_t, error_r = train_loop_handler.validate(i, evaluate_mode=True)
        if tensorboard_logger:
            tensorboard_logger.scalar_summary('valid_error_t', error_t, i , is_train=False)
            tensorboard_logger.scalar_summary('valid_error_r', error_r, i , is_train=False)
            tensorboard_logger.scalar_summary('valid_loss_avg', loss, i , is_train=False)

        # loss,error_t, error_r = train_loop_handler.test(i,  evaluate_mode=True)
        #
        # if tensorboard_logger:
        #     tensorboard_logger.scalar_summary('test_error_t', error_t, i , is_test=True)
        #     tensorboard_logger.scalar_summary('test_error_r', error_r, i , is_test=True)
        #     tensorboard_logger.scalar_summary('test_loss_avg', loss, i , is_test=True)

        # print("Training Begins:")
        # train_loop_handler.loop(epochs, output_path, load_best_checkpoint=start_from_last, save_all_checkpoints=False)
        # print("Training Complete")

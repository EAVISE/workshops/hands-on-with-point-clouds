import argparse
from ulaval_6dof_object_tracking.deeptrack.deeptracker import DeepTracker

import sys
import json
import time
import cv2
import numpy as np

from ulaval_6dof_object_tracking.deeptrack.datalogger import DataLogger
from ulaval_6dof_object_tracking.utils.data import image_blend, angle_distance, compute_axis
import os
from ulaval_6dof_object_tracking.deeptrack.deeptrack_loader import DeepTrackLoader
from pytorch_toolbox.transformations.compose import Compose
from ulaval_6dof_object_tracking.deeptrack.data_augmentation import ToTensor
from PIL import Image
from ulaval_6dof_object_tracking.utils.sequence_loader import SequenceLoader

import math

ESCAPE_KEY = 27


from ulaval_6dof_object_tracking.utils.rgbd_dataset import RGBDDataset
from ulaval_6dof_object_tracking.utils.data import compute_2Dboundingbox, show_frames, normalize_scale, \
    combine_view_transform
from ulaval_6dof_object_tracking.utils.transform import Transform
from ulaval_6dof_object_tracking.deeptrack.data_augmentation import NormalizeChannels, OffsetDepth


def draw_debug(img, pose, gt_pose, tracker, alpha, debug_info):
    if debug_info is not None:
        img_render, bb, _ = debug_info
        img_render = cv2.resize(img_render, (bb[2, 1] - bb[0, 1], bb[1, 0] - bb[0, 0]))
        crop = img[bb[0, 0]:bb[1, 0], bb[0, 1]:bb[2, 1], :]
        h, w, c = crop.shape
        blend = image_blend(img_render[:h, :w, ::-1], crop)
        img[bb[0, 0]:bb[1, 0], bb[0, 1]:bb[2, 1], :] = cv2.addWeighted(img[bb[0, 0]:bb[1, 0], bb[0, 1]:bb[2, 1], :],
                                                                       1 - alpha, blend, alpha, 1)
    else:
        axis = compute_axis(pose, tracker.camera)
        axis_gt = compute_axis(gt_pose, tracker.camera)

        cv2.line(img, tuple(axis_gt[0, ::-1]), tuple(axis_gt[1, ::-1]), (0, 0, 155), 3)
        cv2.line(img, tuple(axis_gt[0, ::-1]), tuple(axis_gt[2, ::-1]), (0, 155, 0), 3)
        cv2.line(img, tuple(axis_gt[0, ::-1]), tuple(axis_gt[3, ::-1]), (155, 0, 0), 3)

        cv2.line(img, tuple(axis[0, ::-1]), tuple(axis[1, ::-1]), (0, 0, 255), 3)
        cv2.line(img, tuple(axis[0, ::-1]), tuple(axis[2, ::-1]), (0, 255, 0), 3)
        cv2.line(img, tuple(axis[0, ::-1]), tuple(axis[3, ::-1]), (255, 0, 0), 3)


def log_pose_difference(prediction, ground_truth, mse, logger):
    prediction_params = prediction.inverse().to_parameters(isDegree=True)
    ground_truth_params = ground_truth.inverse().to_parameters(isDegree=True)
    difference = np.zeros(7)
    for j in range(3):
        difference[j] = abs(prediction_params[j] - ground_truth_params[j])
        difference[j + 3] = abs(angle_distance(prediction_params[j + 3], ground_truth_params[j + 3]))
    difference[6] = mse
    logger.add_row(logger.get_dataframes_id()[0], difference)


def load_from_index(self, path, index):
    rgb = np.array(Image.open(os.path.join(path, index + ".png")))
    depth = np.array(Image.open(os.path.join(path, index + "d.png"))).astype(np.uint16)
    return rgb, depth


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Test DeepTrack')
    parser.add_argument('-c', '--config_file', help="config_file", default="None")
    parser.add_argument('-v', '--verbose', help="verbose",action="store_true")
    args = parser.parse_args()

    with open(args.config_file) as data_file:
        data = json.load(data_file)

    # Populate important data from config file
    OUTPUT_PATH = data["output_path"]
    VIDEO_PATH = data["video_path"]
    MODEL_PATH = data["model_path"]
    model_split_path = MODEL_PATH.split(os.sep)
    model_name = model_split_path[-1]
    model_folder = os.sep.join(model_split_path[:-1])
    MODELS_3D = data["models"]
    SHADER_PATH = data["shader_path"]
    CLOSED_LOOP_ITERATION = int(data["closed_loop_iteration"])
    SAVE_VIDEO = data["save_video"] == "True"
    SAVE_FRAMES = data["save_frames"] == "True"
    SHOW_AXIS = data["show_axis"] == "True"

    OBJECT_WIDTH = int(MODELS_3D[0]["object_width"])
    MODEL_3D_PATH = MODELS_3D[0]["model_path"]
    # try:
    #     MODEL_3D_AO_PATH = MODELS_3D[0]["ambiant_occlusion_model"]
    # except KeyError:
    #     MODEL_3D_AO_PATH = None
    USE_SENSOR = data["use_sensor"] == "True"
    RESET_FREQUENCY = int(data["reset_frequency"])

    # video_data = Dataset(VIDEO_PATH)
    data_path = VIDEO_PATH


    sequence = SequenceLoader(data_path)
    camera = sequence.camera
    detection_mode = False
    debug_info = None

    tracker = DeepTracker(camera, model_folder, OBJECT_WIDTH)
    tracker.load(model_folder, MODEL_PATH, MODEL_3D_PATH, "", SHADER_PATH)
    #tracker.print()

    previous_pose, previous_rgb, previous_depth = sequence[0]

    log_folder = os.path.join(model_folder, "scores")
    if SAVE_VIDEO:
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter(os.path.join(log_folder, "video.avi"), fourcc, 30.0, (camera.width, camera.height))

    data_logger = DataLogger()
    data_logger.create_dataframe("{}_eval".format(model_name), ("Tx", "Ty", "Tz", "Rx", "Ry", "Rz","MSE"))

    #for i in range(1, 2):
    for i in range(1, sequence.size()):

        ground_truth_pose, current_rgb, current_depth = sequence[i]

        screen = current_rgb.copy()
        #RESET_FREQUENCY = 2
        if RESET_FREQUENCY != 0 and i % RESET_FREQUENCY == 0:
            previous_pose = ground_truth_pose
        else:
            # process pose estimation of current frame given last pose
            start_time = time.time()
            if detection_mode:
                previous_pose = ground_truth_pose
            else:
                for j in range(CLOSED_LOOP_ITERATION):
                    #previous_pose = ground_truth_pose
                    predicted_pose, debug_info, MSE = tracker.estimate_current_pose(previous_pose, ground_truth_pose, current_rgb,
                                                                               current_depth, debug=args.verbose)

                    previous_pose = predicted_pose
            print("[{}]Estimation processing time : {}".format(i, time.time() - start_time))
            if not USE_SENSOR:
                log_pose_difference(predicted_pose.inverse(), ground_truth_pose.inverse(), MSE, data_logger)


        if SHOW_AXIS:
            debug_info = None


        draw_debug(screen, previous_pose, ground_truth_pose, tracker, 1, debug_info)
        previous_rgb = current_rgb

        cv2.imshow("Debug", screen[:, :, ::-1])
        if SAVE_VIDEO:
            out.write(screen[:, :, ::-1])
        if SAVE_FRAMES:
            frame_folder = os.path.join(log_folder, "frames")
            if not os.path.exists(frame_folder):
                os.mkdir(frame_folder)
            cv2.imwrite(os.path.join(frame_folder, "{}.jpg".format(i)), screen[:, :, ::-1])
        key = cv2.waitKey(1)
        key_chr = chr(key & 255)
        if key != -1:
            print("pressed key id : {}, char : [{}]".format(key, key_chr))
        if key_chr == " ":
            print("Reset at frame : {}".format(i))
            previous_pose = ground_truth_pose
            #detection_mode = not detection_mode
        if key == ESCAPE_KEY:
            break
    if not os.path.exists(log_folder):
        os.mkdir(log_folder)
    data_logger.save(log_folder)
    if SAVE_VIDEO:
        out.release()

from pytorch_toolbox.io import yaml_load

from ulaval_6dof_object_tracking.utils.transform import Transform
from ulaval_6dof_object_tracking.utils.camera import Camera
from ulaval_6dof_object_tracking.deeptrack.deeptrack_loader_base import DeepTrackLoaderBase
from ulaval_6dof_object_tracking.utils.data import combine_view_transform, show_frames, compute_2Dboundingbox, normalize_scale
from ulaval_6dof_object_tracking.utils.model_renderer import ModelRenderer
from ulaval_6dof_object_tracking.utils.plyparser import PlyParser
from ulaval_6dof_object_tracking.utils.pointcloud import maximum_width
from ulaval_6dof_object_tracking.utils.uniform_sphere_sampler import UniformSphereSampler
from tqdm import tqdm
import argparse
import shutil
import os
import math
import numpy as np
import cv2
from PIL import Image
ESCAPE_KEY = 1048603

def mask_real_image_debug(color, depth, rgb_render, depth_render, mask_more):
    SATURATION_THRESHOLD = 255
    #mask = (depth_render != 0).astype(np.uint8)[:, :, np.newaxis]
    mask = np.all(rgb_render == 0, axis=2)
    #masked_rgb = color * mask
    import matplotlib.pyplot as plt
    fig, axis = plt.subplots()
    axis.imshow(np.squeeze(mask).astype(np.uint8))
    plt.show()

    #mask = np.all(mask == 1, axis=2)
    #show_frames(masked_rgb, depth, masked_rgb, depth)
    #correct_mask=np.squeeze(mask).astype(np.uint8)
    color[np.invert(mask)] = 0
    show_frames(color, depth, color, depth)
    # cv2.imwrite('masked_rgb.png',masked_rgb)
    #k = cv2.waitKey(1)

    masked_hsv = cv2.cvtColor(masked_rgb, cv2.COLOR_BGR2HSV)
    saturation_mask = (masked_hsv[:, :, 2] <= SATURATION_THRESHOLD)[:, :, np.newaxis].astype(np.uint8)
    # cv2.imwrite('saturation_mask.png', saturation_mask)
    total_mask = np.bitwise_and(mask, saturation_mask)
    # cv2.imwrite('total_mask.png', total_mask)
    # from scipy import ndimage
    # ndimage.binary_dilation(total_mask, iterations=20).astype(total_mask.dtype)

    if mask_more:
        kernel = np.ones((5, 5), np.uint8)
        total_mask_depth = cv2.dilate(total_mask, kernel, iterations=1)
        total_mask_depth = total_mask_depth[:, :, np.newaxis]

    masked_color = color * total_mask
    masked_depth = depth[:total_mask.shape[0], :total_mask.shape[1]] * total_mask[:, :, 0]
    #masked_depth = depth[:total_mask_depth.shape[0], :total_mask_depth.shape[1]] * total_mask_depth[:, :, 0]


    # if mask_more:
    #     kernel = np.ones((5, 5), np.uint8)
    #     total_mask_rgb = cv2.dilate(total_mask, kernel, iterations=1)
    #     total_mask_rgb = total_mask_rgb[:, :, np.newaxis]
    #
    # masked_color = color * total_mask_rgb
    # masked_depth = depth[:total_mask.shape[0], :total_mask.shape[1]] * total_mask[:, :, 0]

    cv2.imwrite('masked_color.png', masked_color)
    cv2.imwrite('masked_depth.png', masked_depth)

    # import matplotlib.pyplot as plt
    # fig, axis = plt.subplots(2, 3)
    # ax1, ax2, ax5 = axis[0, :]
    # ax3, ax4, ax6 = axis[1, :]
    # ax1.imshow(masked_rgb.astype(np.uint8))
    # ax2.imshow(masked_hsv.astype(np.uint8))
    # ax3.imshow(np.squeeze(mask).astype(np.uint8))
    # ax4.imshow(np.squeeze(saturation_mask).astype(np.uint8))
    # # ax5.imshow((rgbA - rgbB).sum(axis=2)/3)
    # ax5.imshow(np.squeeze(masked_color).astype(np.uint8))
    # ax6.imshow(np.squeeze(total_mask).astype(np.uint8))
    # plt.show()
    return masked_color, masked_depth

def mask_real_image(color, depth, depth_render, mask_more):
    SATURATION_THRESHOLD = 255
    mask = (depth_render != 0).astype(np.uint8)[:, :, np.newaxis]
    masked_rgb = color * mask
    # cv2.imwrite('masked_rgb.png',masked_rgb)
    #k = cv2.waitKey(1)

    masked_hsv = cv2.cvtColor(masked_rgb, cv2.COLOR_BGR2HSV)
    saturation_mask = (masked_hsv[:, :, 2] <= SATURATION_THRESHOLD)[:, :, np.newaxis].astype(np.uint8)
    # cv2.imwrite('saturation_mask.png', saturation_mask)
    total_mask = np.bitwise_and(mask, saturation_mask)
    # cv2.imwrite('total_mask.png', total_mask)
    # from scipy import ndimage
    # ndimage.binary_dilation(total_mask, iterations=20).astype(total_mask.dtype)

    if mask_more:
        kernel = np.ones((5, 5), np.uint8)
        total_mask_depth = cv2.dilate(total_mask, kernel, iterations=1)
        total_mask_depth = total_mask_depth[:, :, np.newaxis]

    masked_color = color * total_mask
    masked_depth = depth[:total_mask.shape[0], :total_mask.shape[1]] * total_mask[:, :, 0]
    #masked_depth = depth[:total_mask_depth.shape[0], :total_mask_depth.shape[1]] * total_mask_depth[:, :, 0]


    # if mask_more:
    #     kernel = np.ones((5, 5), np.uint8)
    #     total_mask_rgb = cv2.dilate(total_mask, kernel, iterations=1)
    #     total_mask_rgb = total_mask_rgb[:, :, np.newaxis]
    #
    # masked_color = color * total_mask_rgb
    # masked_depth = depth[:total_mask.shape[0], :total_mask.shape[1]] * total_mask[:, :, 0]

    cv2.imwrite('masked_color.png', masked_color)
    cv2.imwrite('masked_depth.png', masked_depth)

    # import matplotlib.pyplot as plt
    # fig, axis = plt.subplots(2, 3)
    # ax1, ax2, ax5 = axis[0, :]
    # ax3, ax4, ax6 = axis[1, :]
    # ax1.imshow(masked_rgb.astype(np.uint8))
    # ax2.imshow(masked_hsv.astype(np.uint8))
    # ax3.imshow(np.squeeze(mask).astype(np.uint8))
    # ax4.imshow(np.squeeze(saturation_mask).astype(np.uint8))
    # # ax5.imshow((rgbA - rgbB).sum(axis=2)/3)
    # ax5.imshow(np.squeeze(masked_color).astype(np.uint8))
    # ax6.imshow(np.squeeze(total_mask).astype(np.uint8))
    # plt.show()
    return masked_color, masked_depth

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Generate synthetic data for DeepTrack')

    parser.add_argument('-c', '--camera', help="camera json path", action="store",
                        default="../utils/camera_configs/synthetic.json")
    parser.add_argument('--shader', help="shader path", action="store", default="../utils/shader")
    parser.add_argument('-o', '--output', help="save path", action="store", default="./generated_data")
    parser.add_argument('-m', '--model', help="model file path", action="store", default="./model_config.yaml")
    parser.add_argument('--real_model', help="whether to use real model", action="store")
    parser.add_argument('--mask_more', help="whether to add borders for mask", action="store")

    parser.add_argument('--real_poses_path', help='use real poses from real sequence', default="None")


    arguments = parser.parse_args()

    SHADER_PATH = arguments.shader
    OUTPUT_PATH = arguments.output
    CAMERA_PATH = arguments.camera


    MODELS = yaml_load(arguments.model)["models"]

    poses = np.load(os.path.join(arguments.real_poses_path,'poses.npy'))

    if not os.path.exists(OUTPUT_PATH):
        os.mkdir(OUTPUT_PATH)
    shutil.copy(arguments.model, os.path.join(OUTPUT_PATH, "models.yml"))

    camera = Camera.load_from_json(CAMERA_PATH)
    window_size = (camera.width, camera.height)
    sphere_sampler = UniformSphereSampler(0.8, 1.4)

    real_dataset = DeepTrackLoaderBase(arguments.real_poses_path)

    # Iterate over all models from config files
    for model in MODELS:
        geometry_path = os.path.join(model["path"], "geometry.ply")
        ao_path = os.path.join(model["path"], "ao.ply")
        vpRender = ModelRenderer(geometry_path, SHADER_PATH, camera, [window_size, window_size])
        if os.path.exists(ao_path):
            vpRender.load_ambiant_occlusion_map(ao_path)
        for i in tqdm(range(len(poses))):
            pair = Transform.from_matrix(poses[i].reshape(4, 4))


            light_intensity = np.zeros(3)
            light_intensity.fill(np.random.uniform(0.1, 1.3))
            light_intensity += np.random.uniform(-0.1, 0.1, 3)
            ambiant_light = np.zeros(3)
            ambiant_light.fill(np.random.uniform(0.5, 1.5))
            vpRender.setup_camera(camera, 0, camera.width, camera.height, 0)
            # rgbB, depthB = vpRender.render_image(pair,
            #                                      fbo_index=0,
            #                                      light_direction=sphere_sampler.random_direction(),
            #                                      light_diffuse=light_intensity,
            #                                      ambiant_light=ambiant_light)
            rgbB, depthB = vpRender.render_image(pair, fbo_index=0)

            if arguments.real_model:
                rgb = np.array(Image.open(os.path.join(arguments.real_poses_path, str(i) + ".png")))
                depth = np.array(Image.open(os.path.join(arguments.real_poses_path, str(i) + "d.png"))).astype(np.uint16)
                rgbB, depthB = mask_real_image_debug(rgb, depth, rgbB, depthB, arguments.mask_more)

            cv2.imwrite(os.path.join(OUTPUT_PATH, '{}.png').format(i), rgbB[:, :, ::-1])
            cv2.imwrite(os.path.join(OUTPUT_PATH, '{}d.png').format(i), depthB.astype(np.uint16))


            cv2.imshow("test", rgbB[:, :, ::-1])
            k = cv2.waitKey(1)
            if k == ESCAPE_KEY:
                break



#!/usr/bin/env bash

SAVE_PATH=../../data/Normal/pen  # Path to save
MODEL_FILE=pen.yml                   # Model file containing all the objects path to render
TRAIN_SAMPLES=20               # Number of training samples
VALID_SAMPLES=5                 # Number of validation samples
TEST_SAMPLES=2                  # Number of test samples
MAX_TRANSLATION=0.02                    # Maximum translation (meter)
MAX_ROTATION=15                         # Maximum rotation (degree)
BOUNDING_BOX=15                          # Bounding box ratio w.r.t. maximum vertex distance in the model (10 => 110% and -10 => 90%)
RESOLUTION=174                          # Resolution of the image samples
DATA_TYPE=png                      # save type : numpy => large but fast to load. png => small put slower to load.

workDIR=`pwd`
#python3 -c "import os;
#print(os.path.dirname(os.path.realpath(__file__)))"

export PYTHONPATH=$PYTHONPATH:${workDIR}/../..    # add your project path to PythonPath
export PYTHONPATH=$PYTHONPATH:${workDIR}/../../../pytorch_toolbox

echo ${PYTHONPATH}

echo Generating training data...
if [ -d ${SAVE_PATH} ]; then
echo "Directory already exists" ;
else
`mkdir -p ${SAVE_PATH}`;
echo "${SAVE_PATH} directory is created"
fi

python3 dataset_generator.py -o ${SAVE_PATH}/train \
                             -m ${MODEL_FILE} \
                             -s ${TRAIN_SAMPLES} \
                              -t ${MAX_TRANSLATION} -r ${MAX_ROTATION} --boundingbox ${BOUNDING_BOX} \
                             -e ${RESOLUTION} --saveformat ${DATA_TYPE} 

echo Generating validation data..
python3 dataset_generator.py -o ${SAVE_PATH}/valid \
                             -m ${MODEL_FILE} \
                             -s ${VALID_SAMPLES} \
                              -t ${MAX_TRANSLATION} -r ${MAX_ROTATION} --boundingbox ${BOUNDING_BOX} \
                             -e ${RESOLUTION} --saveformat ${DATA_TYPE} 
echo Generating test data..
python3 dataset_generator.py -o ${SAVE_PATH}/test \
                             -m ${MODEL_FILE} \
                             -s ${TEST_SAMPLES} \
                              -t ${MAX_TRANSLATION} -r ${MAX_ROTATION} --boundingbox ${BOUNDING_BOX} \
                             -e ${RESOLUTION} --saveformat ${DATA_TYPE} 

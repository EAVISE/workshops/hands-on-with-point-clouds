from pytorch_toolbox.loop_callback_base import LoopCallbackBase
import torch.nn.functional as F
from torch.autograd import Variable
import os
import numpy as np


class DeepTrackCallback(LoopCallbackBase):
    def __init__(self, file_output_path, log_sample_loss = False, is_dof_only=True, update_rate=10):
        self.dof_losses = []
        self.count = 0
        self.update_rate = update_rate
        self.file_output_path = file_output_path
        self.is_dof_only = is_dof_only
        self.components_losses = [[], [], [], [], [], []]
        self.labels = ["tx", "ty", "tz", "rx", "ry", "rz"]
        self.predicted_labels = ["MSE","l-tx", "l-ty", "l-tz", "l-rx", "l-ry", "l-rz"]
        self.target_labels = ["T-tx", "T-ty", "T-tz", "T-rx", "T-ry", "T-rz"]
        self.sample_loss = []
        self.targets_numpy = []
        self.debug_output_path = os.path.join(self.file_output_path,'debug')
        if not os.path.exists(self.debug_output_path):
            os.mkdir(self.debug_output_path)
        self.log_sample_loss = log_sample_loss


    def batch(self, predictions, network_inputs, targets, sample_loss, target_numpy, is_train=True, tensorboard_logger=None):
        prediction = predictions[0].data.clone()
        dof_loss = F.mse_loss(Variable(prediction), Variable(targets[0])).data.item()
        for i in range(6):
            component_loss = F.mse_loss(Variable(prediction[:, i]), Variable(targets[0][:, i])).data.item()
            self.components_losses[i].append(component_loss)
        self.dof_losses.append(dof_loss)
        self.sample_loss.append(sample_loss)
        self.targets_numpy.append(target_numpy)

    def epoch(self, epoch, loss, data_time, batch_time, error_t, error_r, is_train=True, tensorboard_logger=None):
        sample_loss_np = np.concatenate(self.sample_loss, axis=0)
        target_np = np.concatenate(self.targets_numpy, axis=0)
        sample_filename = ("train_sample_loss"+str(epoch)+".npy") if is_train else ("valid_sample_loss"+str(epoch)+".npy")
        target_filename = ("train_target" + str(epoch) + ".npy") if is_train else (
                    "valid_target" + str(epoch) + ".npy")
        if self.log_sample_loss:
            np.save(os.path.join(self.debug_output_path, sample_filename), sample_loss_np)
        if epoch == 1:
            np.save(os.path.join(self.debug_output_path, target_filename), target_np)
            if tensorboard_logger:
                for col in range(np.size(target_np, 1)):
                    tensorboard_logger.histo_summary(self.target_labels[col], target_np[:, col], epoch + 1,
                                                     is_train=is_train)

        losses = [sum(self.dof_losses) / len(self.dof_losses)]
        for label, class_loss in zip(self.labels, self.components_losses):
            avr = sum(class_loss) / len(class_loss)
            losses.append(avr)
            if tensorboard_logger:
                tensorboard_logger.scalar_summary(label, avr, epoch + 1, is_train=is_train)
        losses.append(error_t)
        losses.append(error_r)

        if tensorboard_logger:
            tensorboard_logger.scalar_summary('error_t', error_t, epoch + 1, is_train=is_train)
            tensorboard_logger.scalar_summary('error_r', error_r, epoch + 1, is_train=is_train)

            for col in range(np.size(sample_loss_np, 1)):
                tensorboard_logger.histo_summary(self.predicted_labels[col], sample_loss_np[:,col], epoch + 1, is_train=is_train)

        self.console_print(loss, data_time, batch_time, losses, is_train)
        filename = "training_data.csv" if is_train else "validation_data.csv"
        self.file_print(os.path.join(self.file_output_path, filename),
                        loss, data_time, batch_time, losses)
        self.dof_losses = []
        self.components_losses = [[], [], [], [], [], []]
        self.sample_loss = []
        self.targets_numpy = []

    @staticmethod
    def draw_debug_info(self, img, pose, gt_pose, camera):
        from ulaval_6dof_object_tracking.utils.data import image_blend, angle_distance, compute_axis
        import cv2
        axis = compute_axis(pose, camera)
        axis_gt = compute_axis(gt_pose, camera)

        thickness =3
        cv2.line(img, tuple(axis_gt[0, ::-1]), tuple(axis_gt[1, ::-1]), (0, 0, 155), thickness)
        cv2.line(img, tuple(axis_gt[0, ::-1]), tuple(axis_gt[2, ::-1]), (0, 155, 0), thickness)
        cv2.line(img, tuple(axis_gt[0, ::-1]), tuple(axis_gt[3, ::-1]), (155, 0, 0), thickness)

        cv2.line(img, tuple(axis[0, ::-1]), tuple(axis[1, ::-1]), (0, 0, 255), thickness)
        cv2.line(img, tuple(axis[0, ::-1]), tuple(axis[2, ::-1]), (0, 255, 0), thickness)
        cv2.line(img, tuple(axis[0, ::-1]), tuple(axis[3, ::-1]), (255, 0, 0), thickness)

    def log(self, rgbA, depthA, rgbB, depthB, renderedRGB, renderedDepth, epoch, index, loss, squared_loss, pose_delta,
            losses, errors_translation, errors_rotation, tensorboard_logger=None):
        # plot_save_dir=os.path.join(self.debug_output_path+'/epoch'+str(epoch)+'/')
        # if not os.path.exists(plot_save_dir):
        #     os.mkdir(plot_save_dir)
        #
        # import matplotlib.pyplot as plt
        # from pylab import title
        # import numpy as np
        # fig, axis = plt.subplots(2, 3)
        # ax1, ax2, ax5 = axis[0, :]
        # ax3, ax4, ax6 = axis[1, :]
        # ax1.imshow(rgbA.astype(np.uint8))
        # ax2.imshow(rgbB.astype(np.uint8))
        # ax3.imshow(depthA)
        # loss0 =np.mean(squared_loss)
        # ax2.title.set_text('%.5f' %(loss0) + "," + '%.5f' % (squared_loss[0]) + "," + '%.5f' % (squared_loss[1]) + "," + '%.5f' %(
        #     squared_loss[2]) + "," + '%.5f' %(squared_loss[3]) + "," + '%.5f' %(squared_loss[4]) + "," + '%.5f' %(squared_loss[5]))
        # ax4.imshow(depthB)
        # ax5.imshow(renderedRGB)
        # ax4.title.set_text('%.5f' % (pose_delta[0]) + "," + '%.5f' % (pose_delta[1]) + "," + '%.5f' %(
        #     pose_delta[2]) + "," + '%.5f' %(pose_delta[3]) + "," + '%.5f' %(pose_delta[4]) + "," + '%.5f' %(pose_delta[5]))
        # ax6.imshow(renderedDepth)
        #
        # # self.draw_debug_info(img, pose, gt_pose, camera)
        # #fig.suptitle(str(loss), fontsize=16)
        # filename =str(index)+'.png'
        #
        # plt.savefig(os.path.join(plot_save_dir, filename))
        # if tensorboard_logger:
        #     tensorboard_logger.scalar_summary('test_error_t', errors_translation, epoch + 1, is_test=True)
        #     tensorboard_logger.scalar_summary('test_error_r', errors_rotation, epoch + 1, is_test=True)
        #     tensorboard_logger.scalar_summary('test_loss_avg', losses, epoch + 1, is_test=True)

        pass


import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import seaborn as sns

dir = '/home/yanming/workspace/6dof-tracking/evaluate/plain-allcp-transfer'
ROW_NUMBER=26
# sequence_data = np.genfromtxt(os.path.join(sequence_score_path, 'model_best.pth.tar_eval.csv'), delimiter=',')
training_data = pd.read_csv(os.path.join(dir, 'training_data.csv')).to_numpy()[0:ROW_NUMBER,:]
validation_data = pd.read_csv(os.path.join(dir, 'validation_data.csv')).to_numpy()[0:ROW_NUMBER,:]

train_t = training_data[:,10]
train_r = training_data[:,11]
train_MSE = training_data[:,2]

valid_t = validation_data[:,10]
valid_r = validation_data[:,11]
valid_MSE = validation_data[:,2]

test_t=pd.read_csv(os.path.join(dir, 'test_error_t.csv'))['Value'].to_numpy()[0:ROW_NUMBER]
test_r=pd.read_csv(os.path.join(dir, 'test_error_r.csv'))['Value'].to_numpy()[0:ROW_NUMBER]
test_MSE=pd.read_csv(os.path.join(dir, 'test_loss_avg.csv'))['Value'].to_numpy()[0:ROW_NUMBER]


length = ROW_NUMBER + 1

fig, axis = plt.subplots()
axis.plot(range(1, length), train_MSE, 'r', label='train')
axis.plot(range(1, length), valid_MSE, 'g', label='valid')
axis.plot(range(1, length), test_MSE, 'b', label='test')
axis.title.set_text('MSE VS epoch')
plt.legend(loc="upper right")
plt.xlabel("epoch")
plt.ylabel("MSE")
plt.savefig(os.path.join(dir,'MSE_loss.png'))
plt.show()

fig, axis = plt.subplots()
axis.plot(range(1, length), train_t, 'r', label='train')
axis.plot(range(1, length), valid_t, 'g', label='valid')
axis.plot(range(1, length), test_t, 'b', label='test')
axis.title.set_text('Error_t VS epoch')
plt.legend(loc="upper right")
plt.xlabel("epoch")
plt.ylabel("Error_t(m)")
plt.savefig(os.path.join(dir,'Error_t.png'))
plt.show()

fig, axis = plt.subplots()
axis.plot(range(1, length), train_r, 'r', label='train')
axis.plot(range(1, length), valid_r, 'g', label='valid')
axis.plot(range(1, length), test_r, 'b', label='test')
axis.title.set_text('Error_r VS epoch')
plt.legend(loc="upper right")
plt.xlabel("epoch")
plt.ylabel("Error_r(degree)")
plt.savefig(os.path.join(dir,'Error_r.png'))
plt.show()

# ax2.plot(range(1, length), train_t, 'r', legend='train')
# ax2.plot(range(1, length), valid_t, 'g', legend='valid')
# ax2.plot(range(1, length), test_t, 'b', legend='test')
# ax2.title.set_text('error_t')
#
# ax3.plot(range(1, length), train_r, 'r', legend='train')
# ax3.plot(range(1, length), valid_r, 'g', legend='valid')
# ax3.plot(range(1, length), test_r, 'b', legend='test')
# ax3.title.set_text('error_r')


from ulaval_6dof_object_tracking.utils.data import combine_view_transform, combine_view_transform_, show_frames, \
    compute_2Dboundingbox, \
    normalize_scale, get_relative_prediction
from ulaval_6dof_object_tracking.utils.model_renderer import ModelRenderer
from ulaval_6dof_object_tracking.utils.transform import Transform
from ulaval_6dof_object_tracking.deeptrack.data_augmentation import NormalizeChannels, OffsetDepth, ToTensor
from ulaval_6dof_object_tracking.deeptrack.deeptrack_net import DeepTrackNet
from ulaval_6dof_object_tracking.utils.camera import Camera
from ulaval_6dof_object_tracking.utils.rgbd_dataset import RGBDDataset
from ulaval_6dof_object_tracking.utils.data import add_hsv_noise, depth_blend, gaussian_noise, color_blend

import torch
import time
import numpy as np
import cv2
import yaml
import os


class DeepTracker(object):
    def __init__(self, camera, model_path, object_width=0):
        self.image_size = None
        self.tracker_model = None
        self.translation_range = None
        self.rotation_range = None
        self.mean = None
        self.std = None
        self.debug_rgb = None
        self.debug_background = None
        self.camera = camera
        self.model_path = model_path
        self.object_width = None

        # geometry_path = os.path.join(model["path"], "geometry.ply")
        # ao_path = os.path.join(model["path"], "ao.ply")
        # vpRender = ModelRenderer(geometry_path, SHADER_PATH, dataset.camera, [window_size, IMAGE_SIZE])

    def load(self, model_path, model_file, model_3d_path="", model_3d_ao_path="", shader_path=""):
        # setup pretrained model
        self.tracker_model = DeepTrackNet(image_size=174)
        checkpoint = torch.load(model_file, map_location=torch.device('cpu'))# change to map_location='cuda:0' if GPU usage is desired
        self.tracker_model.load_state_dict(checkpoint['state_dict'])
        epoch = checkpoint['epoch']
        self.tracker_model.eval()


        self.load_parameters_from_model_()
        self.setup_renderer(model_3d_path, shader_path)

    def setup_renderer(self, model_3d_path, shader_path):
        # window = InitOpenGL(*self.image_size)
        self.renderer = ModelRenderer(model_3d_path, shader_path, self.camera,
                                      [(self.camera.width, self.camera.height), self.image_size])
        # self.renderer = ModelRenderer(model_3d_path, shader_path, self.camera, [self.image_size, self.image_size])
        # if model_3d_ao_path is not None:
        #     self.renderer.load_ambiant_occlusion_map(model_3d_ao_path)

    def print(self):
        print(self.tracker_model.model_string())

    def load_parameters_from_model_(self):
        with open(os.path.join(self.model_path, "meta.yml"), 'r') as stream:
            try:
                meta_data = yaml.safe_load(stream)
                print(meta_data)
            except yaml.YAMLError as exc:
                print(exc)

        self.image_size = (
            int(meta_data['image_size']), int(meta_data['image_size']))
        self.translation_range = float(meta_data['translation_range'])
        self.rotation_range = float(meta_data['rotation_range'])
        self.mean = np.load(os.path.join(self.model_path, "mean.npy"))
        self.std = np.load(os.path.join(self.model_path, "std.npy"))
        self.object_width = int(meta_data['bounding_box_width'])

    def set_configs_(self, configs):
        self.tracker_model.set_configs(configs)

    def renderA(self, previous_pose):
        self.renderer.setup_camera(self.camera, 0, self.camera.width, self.camera.height, 0)
        render_rgb, render_depth = self.renderer.render_image(previous_pose, fbo_index=0)
        bb2 = compute_2Dboundingbox(previous_pose, self.camera, self.object_width, scale=(1000, -1000, -1000))
        rgbA, depthA = normalize_scale(render_rgb, render_depth, bb2, self.image_size)
        return rgbA, depthA

    def compute_render(self, previous_pose, bb):
        left = np.min(bb[:, 1])
        right = np.max(bb[:, 1])
        top = np.min(bb[:, 0])
        bottom = np.max(bb[:, 0])
        self.renderer.setup_camera(self.camera, left, right, bottom, top)
        render_rgb, render_depth = self.renderer.render_image(previous_pose, fbo_index=1)

        return render_rgb, render_depth

    def unnormalize_label(self, params, max_translation, max_rotation_rad):
        import math
        params[:, :3] *= max_translation
        params[:, 3:] *= math.degrees(max_rotation_rad)
        return params

    def render_with_predicted_pose(self, current_pose):
        from ulaval_6dof_object_tracking.utils.uniform_sphere_sampler import UniformSphereSampler
        sphere_sampler = UniformSphereSampler(0.8, 1.4)
        light_intensity = np.zeros(3)
        light_intensity.fill(np.random.uniform(0.1, 1.3))
        light_intensity += np.random.uniform(-0.1, 0.1, 3)
        ambiant_light = np.zeros(3)
        ambiant_light.fill(np.random.uniform(0.5, 1.5))

        self.renderer.setup_camera(self.camera, 0, self.camera.width, self.camera.height, 0)
        rgbB, depthB = self.renderer.render_image(current_pose,
                                                  fbo_index=0,
                                                  light_direction=sphere_sampler.random_direction(),
                                                  light_diffuse=light_intensity,
                                                  ambiant_light=ambiant_light)
        rgbB, depthB = normalize_scale(rgbB, depthB, bb, self.image_size)

    def get_image_pair(self, previous_pose, gt_pose, current_rgb, current_depth):
        bb2 = compute_2Dboundingbox(previous_pose, self.camera, self.object_width, scale=(1000, -1000, -1000))
        bb = compute_2Dboundingbox(previous_pose, self.camera, self.object_width, scale=(1000, -1000, -1000))

        rgbA, depthA = self.renderA(previous_pose)
        rgbB, depthB = normalize_scale(current_rgb, current_depth, bb2, self.image_size)

        # previous_pose_para = Transform.to_parameters(previous_pose)
        # gt_pose_para = Transform.to_parameters(gt_pose)
        from numpy.linalg import inv
        mat = previous_pose.matrix.copy()
        mat = inv((mat))
        pose_diff = mat.dot(gt_pose.matrix)

        # random_transfrom = Transform.from_parameters(*pose_diff)
        pose_diff = Transform.from_matrix(pose_diff)

        dt = gt_pose.to_parameters() - previous_pose.to_parameters()
        pose_diff2 = Transform.from_parameters(*dt)
        gt_pose_verify = combine_view_transform_(previous_pose, pose_diff)
        gt_pose_verify2 = combine_view_transform(previous_pose, pose_diff2)
        diff2 = gt_pose_verify2.to_parameters() - previous_pose.to_parameters()
        gt_pose_verify3 = combine_view_transform_(previous_pose, pose_diff2)
        diff3 = (gt_pose_verify3).to_parameters() - previous_pose.to_parameters()
        gt_pose_verify4 = combine_view_transform(previous_pose, pose_diff)

        np.save('initial.npy', previous_pose.matrix)
        # np.save('pose_diff.npy', pose_diff.matrix)
        np.save('pose_diff.npy', pose_diff.matrix)

        return rgbA, depthA, rgbB, depthB, previous_pose, pose_diff

    # def estimate_current_pose_org(self, previous_pose, gt_pose, current_rgb, current_depth, debug=False, debug_time=False,
    #                           add_bg=False, filter_depth=False, filter_rgb=False, dilate_rgb=False):
    #     # prevmat=previous_pose.matrix.copy();
    #     # prevmat[0, 3]=0
    #     # prevmat[1, 3] = 0
    #     # previous_pose_m = Transform.from_matrix(prevmat)
    #     # previous_pose_m=previous_pose
    #     #print("previous_pose rz", previous_pose.to_parameters())
    #     if debug_time:
    #         start_time = time.time()
    #     bb = compute_2Dboundingbox(previous_pose, self.camera, self.object_width, scale=(1000, 1000, -1000))
    #     bb2 = compute_2Dboundingbox(previous_pose, self.camera, self.object_width, scale=(1000, -1000, -1000))
    #     if debug_time:
    #         print("Compute BB : {}".format(time.time() - start_time))
    #         start_time = time.time()
    #     #np.save('previous_pose.npy', previous_pose.matrix)
    #     #rgbA, depthA = self.renderA(previous_pose)
    #     rgbA, depthA = self.compute_render(previous_pose, bb)
    #
    #     if debug_time:
    #         print("Render : {}".format(time.time() - start_time))
    #         start_time = time.time()
    #     rgbB, depthB = normalize_scale(current_rgb, current_depth, bb2, self.image_size)
    #
    #     if dilate_rgb:
    #         kernel = np.ones((3, 3), np.uint8)
    #         rgbB = cv2.dilate(rgbB, kernel, iterations=1)
    #
    #     dilate_rgb_edge = False
    #     if dilate_rgb_edge:
    #         show_frames(rgbA, depthA, rgbB, depthB)
    #         image_with_edges = cv2.Canny(depthB.astype(np.uint8), 100, 200)
    #         import matplotlib.pyplot as plt
    #         # fig, axis = plt.subplots()
    #         # axis.imshow(image_with_edges)
    #         # plt.show()
    #
    #         # find indices
    #         indices = np.transpose(image_with_edges.nonzero())
    #         # select seeds
    #         choices = np.random.randint(indices.shape[0], size=20)
    #         # find neighbours
    #         selection_radius = 15
    #         anchors = []
    #         for i in range(indices.shape[0]):
    #             for choice in choices:
    #                 d = indices[i, :] - indices[choice, :]
    #                 if d[0] * d[0] + d[1] * d[1] < selection_radius:
    #                     anchors.append(indices[i, :])
    #         # set max and min radius of dilation
    #         max_amp = 5
    #         min_amp = 2
    #         for item in anchors:
    #             r = np.random.randint(low=min_amp, high=max_amp, size=1)
    #             xmn = int(item[0] - r)
    #             xmx = int(item[0] + r)
    #             ymn = int(item[1] - r)
    #             ymx = int(item[1] + r)
    #             zone = rgbB[xmn:xmx, ymn:ymx]
    #             zone[zone == 0] = zone[np.unravel_index(np.argmax(np.sum(zone, axis=2)), zone.shape)]
    #             rgbB[xmn:xmx, ymn:ymx] = zone
    #
    #         #show_frames(rgbA, depthA, rgbB, depthB)
    #
    #     if filter_rgb:
    #         show_frames(rgbA, depthA, rgbB, depthB)
    #         image_with_edges = cv2.Canny(depthB.astype(np.uint8), 100, 200)
    #         import matplotlib.pyplot as plt
    #         # fig, axis = plt.subplots()
    #         # axis.imshow(image_with_edges)
    #         # plt.show()
    #
    #         # find indices
    #         indices = np.transpose(image_with_edges.nonzero())
    #         # select seeds
    #         choices = np.random.randint(indices.shape[0], size=20)
    #         # find neighbours
    #         selection_radius = 15
    #         anchors = []
    #         for i in range(indices.shape[0]):
    #             for choice in choices:
    #                 d = indices[i, :] - indices[choice, :]
    #                 if d[0] * d[0] + d[1] * d[1] < selection_radius:
    #                     anchors.append(indices[i, :])
    #         # set max and min radius of dilation
    #         max_amp = 5
    #         min_amp = 2
    #         for item in anchors:
    #             r = np.random.randint(low=min_amp, high=max_amp, size=1)
    #             xmn = int(item[0] - r)
    #             xmx = int(item[0] + r)
    #             ymn = int(item[1] - r)
    #             ymx = int(item[1] + r)
    #             zone = rgbB[xmn:xmx, ymn:ymx]
    #             zone[zone == 0] = zone[np.unravel_index(np.argmax(np.sum(zone, axis=2)), zone.shape)]
    #             rgbB[xmn:xmx, ymn:ymx] = zone
    #
    #         #show_frames(rgbA, depthA, rgbB, depthB)
    #
    #     if filter_depth:
    #         show_frames(rgbA, depthA, rgbB, depthB)
    #         image_with_edges = cv2.Canny(depthB.astype(np.uint8), 100, 200)
    #         import matplotlib.pyplot as plt
    #         # fig, axis = plt.subplots()
    #         # axis.imshow(image_with_edges)
    #         # plt.show()
    #
    #         # find indices
    #         indices = np.transpose(image_with_edges.nonzero())
    #         # select seeds
    #         choices = np.random.randint(indices.shape[0], size=20)
    #         # find neighbours
    #         selection_radius = 15
    #         anchors = []
    #         for i in range(indices.shape[0]):
    #             for choice in choices:
    #                 d = indices[i, :] - indices[choice, :]
    #                 if d[0] * d[0] + d[1] * d[1] < selection_radius:
    #                     anchors.append(indices[i, :])
    #         # set max and min radius of dilation
    #         max_amp = 8
    #         min_amp = 3
    #         for item in anchors:
    #             r = np.random.randint(low=min_amp, high=max_amp, size=1)
    #         xmn = int(item[0] - r)
    #         xmx = int(item[0] + r)
    #         ymn = int(item[1] - r)
    #         ymx = int(item[1] + r)
    #         zone = depthB[xmn:xmx, ymn:ymx]
    #         maxp = np.max(zone)
    #         zone[zone == 0] = maxp
    #         depthB[xmn:xmx, ymn:ymx] = zone
    #
    #         #show_frames(rgbA, depthA, rgbB, depthB)
    #         # kernel = np.ones((3, 3), np.uint8)
    #         # depthB_erode = cv2.dilate(depthB, kernel, iterations=3)
    #         # # depthB_erode=depthB_erode[:,:,np.newaxis]
    #         # show_frames(rgbA, depthA, rgbB, depthB_erode)
    #     if add_bg:
    #         dir = "/home/yanming/workspace/6dof-tracking/configs"
    #         img = "bg"
    #         bg = RGBDDataset(dir)
    #         color_background, depth_background = bg.load_specific_image(dir, img, 450, 540, rgbB.shape[1])
    #         depth_background = depth_background.astype(np.int32)
    #         #show_frames(rgbA, depthA, rgbB, depthB)
    #         #show_frames(rgbB, depthB, color_background, depth_background)
    #         rgbB, depthB = color_blend(rgbB, depthB, color_background, depth_background)
    #         #show_frames(rgbA, depthA, rgbB, depthB)
    #
    #     rgbA_copy = np.copy(rgbA)
    #     rgbB_copy = np.copy(rgbB)
    #     depthA_copy = np.copy(depthA)
    #     depthB_copy = np.copy(depthB)
    #
    #     # show_frames(rgbA, depthA, rgbB, depthB)
    #     debug_info = (rgbA, bb2, np.hstack((rgbA, rgbB)))
    #     rgbB_copy = np.copy(rgbB)
    #
    #     rgbA = rgbA.astype(np.float)
    #     rgbB = rgbB.astype(np.float)
    #     depthA = depthA.astype(np.float)
    #     depthB = depthB.astype(np.float)
    #
    #     depthA = OffsetDepth.normalize_depth(depthA, previous_pose)
    #     depthB = OffsetDepth.normalize_depth(depthB, previous_pose)
    #
    #     # show_frames(rgbA, depthA, rgbB, depthB)
    #
    #     # if debug:
    #     #     show_frames(rgbA, depthA, rgbB, depthB)
    #     rgbA, depthA = NormalizeChannels.normalize_channels(rgbA, depthA, self.mean[:4], self.std[:4])
    #     rgbB, depthB = NormalizeChannels.normalize_channels(rgbB, depthB, self.mean[4:], self.std[4:])
    #
    #     rgbAa = np.zeros((174, 174, 3), np.uint8)
    #     rgbBb = np.zeros((174, 174, 3), np.uint8)
    #
    #     for ii in range(3):
    #         rgbAa[:, :, ii] = rgbA[2 - ii, :, :]
    #     rgbBb[:, :, ii] = rgbB[2 - ii, :, :]
    #
    #     # show_frames(rgbAa, depthA, rgbBb, depthB)
    #
    #     # depthA = depthA[np.newaxis, :]
    #     # depthB = depthB[np.newaxis, :]
    #     #
    #     # prev_image = np.vstack((rgbA, depthA))
    #     # current_image = np.vstack((rgbB, depthB))
    #
    #     if debug_time:
    #         print("Normalize : {}".format(time.time() - start_time))
    #     start_time = time.time()
    #
    #     data_org = rgbA, depthA, rgbB, depthB, [Transform.to_parameters(previous_pose), 0.]
    #     totensor = ToTensor()
    #     data = totensor(data_org)
    #     data0 = data[0].view((1, 4, 174, 174))
    #     data1 = data[1].view((1, 4, 174, 174))
    #     data = []
    #     data.append(data0)
    #     data.append(data1)
    #     # prev_image_tensor = ToTensor(prev_image)
    #     # current_image_tensor = toTensor(current_image)
    #     # data = []
    #     # data.append(prev_image_tensor)
    #     # data.append(current_image_tensor)
    #     # data_var, target_var = self.to_autograd(data, targetBatch, is_train=False)
    #     # with torch.no_grad():
    #     #     y_pred = self.predict(data_var)
    #     self.tracker_model.eval()
    #     self.tracker_model.cuda()
    #     with torch.no_grad():
    #         prediction_org = self.tracker_model(data0.cuda(), data1.cuda()).cpu()
    #     prediction_copy = np.copy(prediction_org)
    #     if debug_time:
    #         print("Network time : {}".format(time.time() - start_time))
    #
    #     gt_pose_para = Transform.to_parameters(gt_pose)
    #     gt_pose_para_degree = Transform.to_parameters(gt_pose, isDegree=True)
    #
    #     prediction = np.copy(prediction_org[0])
    #     prediction_tmp = np.copy(prediction_org[0])
    #     prediction[:3] *= float(self.translation_range)
    #     prediction[3:] *= float(self.rotation_range)
    #
    #     prediction_tmp[:3] *= float(self.translation_range)
    #     prediction_tmp[3:] *= float(self.rotation_range)
    #     prediction_tmp[3:] = prediction_tmp[3:] * 180.0 / np.pi
    #
    #     # prediction[0][0] -= previous_pose.matrix[0, 3]
    #     # prediction[0][1] -= previous_pose.matrix[1, 3]
    #
    #     # prediction = self.unnormalize_label(prediction, self.translation_range, self.rotation_range)
    #     # if debug:
    #     # print("Prediction : {}".format(prediction))
    #     prediction = Transform.from_parameters(*prediction)
    #     prediction_tmp = Transform.from_parameters(*prediction_tmp, is_degree=True)
    #     # print("prediction=", prediction.to_parameters())
    #     # print("prediction_tmp=", prediction_tmp.to_parameters())
    #     # prediction = Transform.from_parameters(0,0,0,0,0,0, is_degree=False)
    #     current_pose = combine_view_transform(previous_pose, prediction)
    #     # current_pose = combine_view_transform(prediction, previous_pose)
    #     # current_pose_tmp = combine_view_transform(previous_pose, prediction_tmp)
    #     current_pose_para = Transform.to_parameters(current_pose)
    #
    #     previous_pose_para = Transform.to_parameters(previous_pose, isDegree=False)
    #
    #     delta_ground_truth_lable = get_relative_prediction(previous_pose, gt_pose)
    #     delta_ground_truth = Transform.to_parameters(delta_ground_truth_lable)  # gt_pose_para - previous_pose_para
    #
    #     # from numpy.linalg import inv
    #     # mat = previous_pose.matrix.copy()
    #     # mat = inv((mat))
    #     # delta_ground_truth = mat.dot(gt_pose.matrix)
    #     # delta_ground_truth = Transform.from_matrix(delta_ground_truth)
    #     # delta_ground_truth=Transform.to_parameters(delta_ground_truth)
    #
    #     delta_ground_truth_output = delta_ground_truth.copy()
    #
    #     # if np.max(np.abs(delta_ground_truth))>np.pi:
    #     #     print("break")
    #     #     delta_ground_truth[delta_ground_truth > np.pi] = delta_ground_truth[delta_ground_truth > np.pi] - np.pi
    #     #     delta_ground_truth[delta_ground_truth < -np.pi] = delta_ground_truth[delta_ground_truth < -np.pi] + np.pi
    #
    #     delta_ground_truth[:3] /= float(self.translation_range)
    #     delta_ground_truth[3:] /= float(self.rotation_range)
    #
    #     squared_delta = np.square(delta_ground_truth - prediction_copy)
    #     MSE = np.square(np.subtract(delta_ground_truth, prediction_copy)).mean()
    #
    #     if MSE > 60:
    #         print("error")
    #
    #     bbNew = compute_2Dboundingbox(current_pose, self.camera, self.object_width, scale=(1000, 1000, -1000))
    #     rgbANew, depthANew = self.compute_render(current_pose, bbNew)
    #     bbNew2 = compute_2Dboundingbox(current_pose, self.camera, self.object_width, scale=(1000, -1000, -1000))
    #     debug_infoNew = (rgbANew, bbNew2, np.hstack((rgbANew, rgbB_copy)))
    #     return current_pose, debug_infoNew, MSE, squared_delta, delta_ground_truth_output, rgbA_copy, depthA_copy, rgbB_copy, depthB_copy, delta_ground_truth_lable

    def estimate_current_pose(self, previous_pose, current_rgb, current_depth, debug=False, debug_time=False):

        if debug_time:
            start_time = time.time()
        bb = compute_2Dboundingbox(previous_pose, self.camera, self.object_width, scale=(1000, 1000, -1000))
        bb2 = compute_2Dboundingbox(previous_pose, self.camera, self.object_width, scale=(1000, -1000, -1000))
        if debug_time:
            print("Compute BB : {}".format(time.time() - start_time))
            start_time = time.time()

        rgbA, depthA = self.compute_render(previous_pose, bb)

        if debug_time:
            print("Render : {}".format(time.time() - start_time))
            start_time = time.time()
        rgbB, depthB = normalize_scale(current_rgb, current_depth, bb2, self.image_size)
        debug_info = (rgbA, bb2, np.hstack((rgbA, rgbB)))

        rgbA = rgbA.astype(np.float)
        rgbB = rgbB.astype(np.float)
        depthA = depthA.astype(np.float)
        depthB = depthB.astype(np.float)

        depthA = OffsetDepth.normalize_depth(depthA, previous_pose)
        depthB = OffsetDepth.normalize_depth(depthB, previous_pose)

        # show_frames(rgbA, depthA, rgbB, depthB)

        if debug:
            show_frames(rgbA, depthA, rgbB, depthB)
        rgbA, depthA = NormalizeChannels.normalize_channels(rgbA, depthA, self.mean[:4], self.std[:4])
        rgbB, depthB = NormalizeChannels.normalize_channels(rgbB, depthB, self.mean[4:], self.std[4:])

        if debug_time:
            print("Normalize : {}".format(time.time() - start_time))
        start_time = time.time()

        data_org = rgbA, depthA, rgbB, depthB, [Transform.to_parameters(previous_pose), 0.]
        totensor = ToTensor()
        data = totensor(data_org)
        data0 = data[0].view((1, 4, 174, 174))
        data1 = data[1].view((1, 4, 174, 174))
        data = []
        data.append(data0)
        data.append(data1)
        # prev_image_tensor = ToTensor(prev_image)
        # current_image_tensor = toTensor(current_image)
        # data = []
        # data.append(prev_image_tensor)
        # data.append(current_image_tensor)
        # data_var, target_var = self.to_autograd(data, targetBatch, is_train=False)
        # with torch.no_grad():
        #     y_pred = self.predict(data_var)
        self.tracker_model.eval()

        with torch.no_grad():
            prediction_org = self.tracker_model(data0, data1)
        prediction_copy = np.copy(prediction_org)
        if debug_time:
            print("Network time : {}".format(time.time() - start_time))


        prediction = np.copy(prediction_org[0])
        prediction_tmp = np.copy(prediction_org[0])
        prediction[:3] *= float(self.translation_range)
        prediction[3:] *= float(self.rotation_range)

        prediction_tmp[:3] *= float(self.translation_range)
        prediction_tmp[3:] *= float(self.rotation_range)
        prediction_tmp[3:] = prediction_tmp[3:] * 180.0 / np.pi

        print('pose change:',prediction)
        prediction = Transform.from_parameters(*prediction)
        #print('prediction after from', prediction)
        prediction_tmp = Transform.from_parameters(*prediction_tmp, is_degree=True)

        current_pose = combine_view_transform(previous_pose, prediction)
        current_pose_para = Transform.to_parameters(current_pose)


        return current_pose, debug_info

    def estimate_current_pose_nodebug(self, previous_pose, current_rgb, current_depth, debug=False, debug_time=False):

        if debug_time:
            start_time = time.time()
        bb = compute_2Dboundingbox(previous_pose, self.camera, self.object_width, scale=(1000, 1000, -1000))
        bb2 = compute_2Dboundingbox(previous_pose, self.camera, self.object_width, scale=(1000, -1000, -1000))
        if debug_time:
            print("Compute BB : {}".format(time.time() - start_time))
            start_time = time.time()

        rgbA, depthA = self.compute_render(previous_pose, bb)

        if debug_time:
            print("Render : {}".format(time.time() - start_time))
            start_time = time.time()
        rgbB, depthB = normalize_scale(current_rgb, current_depth, bb2, self.image_size)
        debug_info = (rgbA, bb2, np.hstack((rgbA, rgbB)))

        rgbA = rgbA.astype(np.float)
        rgbB = rgbB.astype(np.float)
        depthA = depthA.astype(np.float)
        depthB = depthB.astype(np.float)

        depthA = OffsetDepth.normalize_depth(depthA, previous_pose)
        depthB = OffsetDepth.normalize_depth(depthB, previous_pose)

        # show_frames(rgbA, depthA, rgbB, depthB)

        # if debug:
        #     show_frames(rgbA, depthA, rgbB, depthB)
        rgbA, depthA = NormalizeChannels.normalize_channels(rgbA, depthA, self.mean[:4], self.std[:4])
        rgbB, depthB = NormalizeChannels.normalize_channels(rgbB, depthB, self.mean[4:], self.std[4:])

        if debug_time:
            print("Normalize : {}".format(time.time() - start_time))
        start_time = time.time()

        data_org = rgbA, depthA, rgbB, depthB, [Transform.to_parameters(previous_pose), 0.]
        totensor = ToTensor()
        data = totensor(data_org)
        data0 = data[0].view((1, 4, 174, 174))
        data1 = data[1].view((1, 4, 174, 174))
        data = []
        data.append(data0)
        data.append(data1)
        # prev_image_tensor = ToTensor(prev_image)
        # current_image_tensor = toTensor(current_image)
        # data = []
        # data.append(prev_image_tensor)
        # data.append(current_image_tensor)
        # data_var, target_var = self.to_autograd(data, targetBatch, is_train=False)
        # with torch.no_grad():
        #     y_pred = self.predict(data_var)
        self.tracker_model.eval()

        with torch.no_grad():
            prediction_org = self.tracker_model(data0.cuda(), data1.cuda()).cpu()
        prediction_copy = np.copy(prediction_org)
        if debug_time:
            print("Network time : {}".format(time.time() - start_time))


        prediction = np.copy(prediction_org[0])
        prediction_tmp = np.copy(prediction_org[0])
        prediction[:3] *= float(self.translation_range)
        prediction[3:] *= float(self.rotation_range)

        prediction_tmp[:3] *= float(self.translation_range)
        prediction_tmp[3:] *= float(self.rotation_range)
        prediction_tmp[3:] = prediction_tmp[3:] * 180.0 / np.pi


        prediction = Transform.from_parameters(*prediction)
        prediction_tmp = Transform.from_parameters(*prediction_tmp, is_degree=True)

        current_pose = combine_view_transform(previous_pose, prediction)
        current_pose_para = Transform.to_parameters(current_pose)


        return current_pose, debug_info


    def estimate_current_pose_old(self, previous_pose, gt_pose, current_rgb, current_depth, debug=False,
                                  debug_time=False):
        print("previous_pose rz", previous_pose.to_parameters())
        if debug_time:
            start_time = time.time()
        bb = compute_2Dboundingbox(previous_pose, self.camera, self.object_width, scale=(1000, -1000, -1000))
        bb2 = compute_2Dboundingbox(previous_pose, self.camera, self.object_width, scale=(1000, -1000, -1000))
        if debug_time:
            print("Compute BB : {}".format(time.time() - start_time))
            start_time = time.time()
        np.save('previous_pose.npy', previous_pose.matrix)
        rgbA, depthA = self.renderA(previous_pose)
        # rgbA, depthA = self.compute_render(previous_pose, bb)

        if debug_time:
            print("Render : {}".format(time.time() - start_time))
            start_time = time.time()
        rgbB, depthB = normalize_scale(current_rgb, current_depth, bb2, self.image_size)

        rgbA_copy = np.copy(rgbA)
        rgbB_copy = np.copy(rgbB)
        depthA_copy = np.copy(depthA)
        depthB_copy = np.copy(depthB)

        # show_frames(rgbA, depthA, rgbB, depthB)
        debug_info = (rgbA, bb2, np.hstack((rgbA, rgbB)))
        rgbB_copy = np.copy(rgbB)

        rgbA = rgbA.astype(np.float)
        rgbB = rgbB.astype(np.float)
        depthA = depthA.astype(np.float)
        depthB = depthB.astype(np.float)

        depthA = OffsetDepth.normalize_depth(depthA, previous_pose)
        depthB = OffsetDepth.normalize_depth(depthB, previous_pose)

        # if debug:
        #     show_frames(rgbA, depthA, rgbB, depthB)
        rgbA, depthA = NormalizeChannels.normalize_channels(rgbA, depthA, self.mean[:4], self.std[:4])
        rgbB, depthB = NormalizeChannels.normalize_channels(rgbB, depthB, self.mean[4:], self.std[4:])

        # depthA = depthA[np.newaxis, :]
        # depthB = depthB[np.newaxis, :]
        #
        # prev_image = np.vstack((rgbA, depthA))
        # current_image = np.vstack((rgbB, depthB))

        if debug_time:
            print("Normalize : {}".format(time.time() - start_time))
            start_time = time.time()

        data_org = rgbA, depthA, rgbB, depthB, Transform.to_parameters(previous_pose)
        totensor = ToTensor()
        data = totensor(data_org)
        data0 = data[0].view((1, 4, 174, 174))
        data1 = data[1].view((1, 4, 174, 174))
        data = []
        data.append(data0)
        data.append(data1)

        self.tracker_model.eval()
        with torch.no_grad():
            prediction = self.tracker_model(data0, data1)
        prediction_copy = np.copy(prediction)
        if debug_time:
            print("Network time : {}".format(time.time() - start_time))

        gt_pose_para = Transform.to_parameters(gt_pose)

        prediction = prediction[0]
        prediction[:3] *= float(self.translation_range)
        prediction[3:] *= float(self.rotation_range)

        # prediction[0][0] -= previous_pose.matrix[0, 3]
        # prediction[0][1] -= previous_pose.matrix[1, 3]

        # prediction = self.unnormalize_label(prediction, self.translation_range, self.rotation_range)
        # if debug:
        # print("Prediction : {}".format(prediction))
        prediction = Transform.from_parameters(*prediction)
        print("prediction=", prediction.to_parameters())
        # prediction = Transform.from_parameters(0,0,0,0,0,0, is_degree=False)
        current_pose = combine_view_transform_(previous_pose, prediction)
        current_pose_para = Transform.to_parameters(current_pose)

        previous_pose_para = Transform.to_parameters(previous_pose, isDegree=False)

        # delta_ground_truth = gt_pose_para-previous_pose_para
        from numpy.linalg import inv
        mat = previous_pose.matrix.copy()
        mat = inv((mat))
        delta_ground_truth = mat.dot(gt_pose.matrix)
        delta_ground_truth = Transform.from_matrix(delta_ground_truth)
        delta_ground_truth = Transform.to_parameters(delta_ground_truth)

        delta_ground_truth_output = delta_ground_truth.copy()

        if np.max(np.abs(delta_ground_truth)) > np.pi:
            print("break")
            delta_ground_truth[delta_ground_truth > np.pi] = delta_ground_truth[delta_ground_truth > np.pi] - np.pi
            delta_ground_truth[delta_ground_truth < -np.pi] = delta_ground_truth[delta_ground_truth < -np.pi] + np.pi

        delta_ground_truth[:3] /= float(self.translation_range)
        delta_ground_truth[3:] /= float(self.rotation_range)

        squared_delta = np.square(delta_ground_truth - prediction_copy)
        MSE = np.square(np.subtract(delta_ground_truth, prediction_copy)).mean()

        if MSE > 60:
            print("error")

        bbNew = compute_2Dboundingbox(current_pose, self.camera, self.object_width, scale=(1000, -1000, -1000))
        rgbANew, depthANew = self.compute_render(current_pose, bbNew)
        bbNew2 = compute_2Dboundingbox(current_pose, self.camera, self.object_width, scale=(1000, -1000, -1000))
        debug_infoNew = (rgbANew, bbNew2, np.hstack((rgbANew, rgbB_copy)))
        return current_pose, debug_infoNew, MSE, squared_delta, delta_ground_truth_output, rgbA_copy, depthA_copy, rgbB_copy, depthB_copy

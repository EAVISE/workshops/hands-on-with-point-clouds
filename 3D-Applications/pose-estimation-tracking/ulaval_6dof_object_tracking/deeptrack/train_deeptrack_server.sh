#!/usr/bin/env bash

DATA_PATH=../../clock_pn_200k_b15_b128
OUTPUT_PATH=../../outputClock_pn_200k_b15_b128
BACKGROUND_PATH=../../../dataset/Laval/sun3d
OCCLUDER_PATH=../../../dataset/Laval/hand

workDIR=`pwd`
#python3 -c "import os;
#print(os.path.dirname(os.path.realpath(__file__)))"

export PYTHONPATH=$PYTHONPATH:${workDIR}/../..    # add your project path to PythonPath
export PYTHONPATH=$PYTHONPATH:${workDIR}/../../../pytorch_toolbox

echo ${PYTHONPATH}

#echo Compute mean/std of dataset
python3 dataset_mean.py -d $DATA_PATH -b $BACKGROUND_PATH -r $OCCLUDER_PATH 

#echo Start training
python3 train.py \
        --output $OUTPUT_PATH/$1 \
       --dataset $DATA_PATH/$1 \
       --background $BACKGROUND_PATH \
        --occluder $OCCLUDER_PATH \
        --model clock_server.yml \
        --e 30 \
        --scheduler \
       --batchsize 128 \
       --save_sample_loss 


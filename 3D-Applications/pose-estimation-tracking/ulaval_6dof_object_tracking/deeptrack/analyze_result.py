import matplotlib.pyplot as plt
import numpy as np
import os

data_path ='/home/yanming/workspace/test/output_l_0.001'
train = np.genfromtxt(os.path.join(data_path, 'training_data.csv'),delimiter=',')
valid = np.genfromtxt(os.path.join(data_path, 'validation_data.csv'),delimiter=',')

fig, ax = plt.subplots()
ax.plot(range(1,len(train)+1), train[:,3], label='training loss')
ax.plot(range(1,len(train)+1), valid[:,3], 'r', label='validation loss')
legend = ax.legend(loc='upper right', shadow=True, fontsize='x-large')
#ax.title.set_text('MSE loss')
plt.title('MSE loss', fontsize = 18)
plt.show()


fig, ax = plt.subplots(2,3)
ax1, ax2, ax5 = ax[0, :]
ax3, ax4, ax6 = ax[1, :]

def plot_component_loss(ax,train,valid,column_number,title):
    ax.plot(range(1, len(train) + 1), train[:, column_number], label='training loss')
    ax.plot(range(1, len(train) + 1), valid[:, column_number], 'r', label='validation loss')
    legend = ax.legend(loc='upper right', shadow=True, fontsize='x-small')
    ax.title.set_text(title)

plot_component_loss(ax1,train,valid,4,'MSE loss - Tx')
plot_component_loss(ax2,train,valid,5,'MSE loss - Ty')
plot_component_loss(ax3,train,valid,6,'MSE loss - Tz')
plot_component_loss(ax4,train,valid,7,'MSE loss - Rx')
plot_component_loss(ax5,train,valid,8,'MSE loss - Ry')
plot_component_loss(ax6,train,valid,9,'MSE loss - Rz')

#plt.title('MSE loss - Tx', fontsize = 18)
plt.show()
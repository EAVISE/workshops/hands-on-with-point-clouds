""" Train loop boilerplate code

    Uses preinstantiated data loaders, network, loss and optimizer to train a model.

    - Supports multiple inputs
    - Supports multiple outputs

"""
import datetime

from pytorch_toolbox.utils import AverageMeter
import time
import torch
from tqdm import tqdm
import numpy as np
import os
from ulaval_6dof_object_tracking.deeptrack.deeptrack_loader import DeepTrackLoader
from ulaval_6dof_object_tracking.utils.transform import Transform
from ulaval_6dof_object_tracking.utils.data import combine_view_transform
from ulaval_6dof_object_tracking.utils.model_renderer import ModelRenderer
from ulaval_6dof_object_tracking.utils.camera import Camera
from ulaval_6dof_object_tracking.utils.data import combine_view_transform, combine_view_transform_, show_frames, \
    compute_2Dboundingbox, normalize_scale
from ulaval_6dof_object_tracking.evaluate_sequence import translation_distance, rotation_distance, eval_pose_error
from ulaval_6dof_object_tracking.deeptrack.deeptrack_callback import DeepTrackCallback
class TrainLoop:

    def __init__(self, model, train_data_loader, valid_data_loader, test_dataset, optimizers, backend,
                 gradient_clip=False,
                 use_tensorboard=False, tensorboard_log_path="./logs", scheduler=None):
        """
        See examples/classification/train.py for usage

        :param model:               Any NetworkBase (in pytorch_toolbox.network) (it is the network model to train)
        :param train_data_loader:   Any torch dataloader for training data
        :param valid_data_loader:   Any torch dataloader for validation data
        :param optimizers:          List of any torch optimizers, the order is important though
        :param backend:             cuda | cpu
        :param scheduler:           Adjust learning rate
        """
        self.train_data = train_data_loader
        self.valid_data = valid_data_loader
        self.test_data = test_dataset
        self.optims = optimizers if type(optimizers) is list else [optimizers]
        self.backend = backend
        self.model = model
        self.gradient_clip = gradient_clip
        self.use_tensorboard = use_tensorboard
        self.tensorboard_logger = None
        self.scheduler = scheduler
        if self.use_tensorboard:
            from pytorch_toolbox.visualization.tensorboard_logger import TensorboardLogger
            date_str = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
            self.tensorboard_logger = TensorboardLogger(os.path.join(tensorboard_log_path, date_str))

        self.callbacks = []
        if backend == "cuda":
            self.model = self.model.cuda()

        #self.setup_renderer()
        self.log_sample_loss=True
        self.sample_loss_file = None
        self.error = np.array([])
        self.use_cyclic_lr = False

    def setup_renderer(self, metadata, model_3d_path, CAMERA_PATH = '../utils/camera_configs/synthetic.json'):
        geometry_path = model_3d_path

        shader_path = '../utils/shader'
        from ulaval_6dof_object_tracking.utils.uniform_sphere_sampler import UniformSphereSampler
        self.camera = Camera.load_from_json(CAMERA_PATH)
        self.window_size = (self.camera.width, self.camera.height)
        min_radius = float(metadata["min_radius"])
        max_radius = float(metadata["max_radius"])
        self.sphere_sampler = UniformSphereSampler(min_radius, max_radius)
        image_size = int(metadata["image_size"])
        self.IMAGE_SIZE = (image_size, image_size)
        self.OBJECT_WIDTH = int(metadata["bounding_box_width"])
        self.translation_range = float(metadata["translation_range"])
        self.rotation_range = float(metadata["rotation_range"])

        self.vpRender = ModelRenderer(geometry_path, shader_path, self.camera, [self.window_size, self.IMAGE_SIZE])
        self.light_intensity = np.zeros(3)
        self.light_intensity.fill(np.random.uniform(0.1, 1.3))
        self.light_intensity += np.random.uniform(-0.1, 0.1, 3)
        self.ambiant_light = np.zeros(3)
        self.ambiant_light.fill(np.random.uniform(0.5, 1.5))
        self.vpRender.setup_camera(self.camera, 0, self.camera.width, self.camera.height, 0)

    @staticmethod
    def setup_loaded_data(data, target, backend):
        """
        Will make sure that the targets are formated as list in the right backend
        :param data:
        :param target:
        :param backend: cuda | cpu
        :return:
        """
        if not isinstance(data, list):
            data = [data]

        if not isinstance(target, list):
            target = [target]

        if backend == "cuda":
            for i in range(len(data)):
                data[i] = data[i].cuda()
            for i in range(len(target)):
                target[i] = target[i].cuda()
        else:
            for i in range(len(data)):
                data[i] = data[i].cpu()
            for i in range(len(target)):
                target[i] = target[i].cpu()
        return data, target

    @staticmethod
    def to_autograd(data, target, is_train=True):
        """
        Converts data and target to autograd Variable
        :param data:
        :param target:
        :return:
        """
        target_var = []
        data_var = []
        for i in range(len(data)):
            v = torch.autograd.Variable(data[i])
            data_var.append(v)
        for i in range(len(target)):
            v = torch.autograd.Variable(target[i])
            target_var.append(v)

        return data_var, target_var

    def predict(self, data_variable):
        """
        compute prediction
        :param data_variable: tuple containing the network's input data
        :return:
        """
        y_pred = self.model(*data_variable)
        if not isinstance(y_pred, tuple):
            y_pred = (y_pred,)
        return y_pred

    def add_callback(self, func):
        """
        take a callback that will be called during the training process. See pytorch_toolbox.loop_callback_base

        :param func:
        :return:
        """
        if isinstance(func, list):
            for cb in func:
                self.callbacks.append(cb)
        else:
            self.callbacks.append(func)

    def calculate_error(self, initial_pose_batch, prediction_batch, target_batch):
        # initial_pose_batch = np.copy(initial_pose_batch_in.cpu())
        # prediction_batch = np.copy(prediction_batch_in.cpu())
        # target_batch = np.copy(target_batch_in.cpu())
        error=np.empty((0,2), float)
        for i in range(0, len(initial_pose_batch)):
            initial_pose = initial_pose_batch[i].cpu().numpy()
            prediction = prediction_batch[i].cpu().numpy()
            target = target_batch[i].cpu().numpy()
            prediction[:3] *= float(self.translation_range)
            prediction[3:] *= float(self.rotation_range)
            final_prediction = Transform.from_parameters(*prediction)
            target[:3] *= float(self.translation_range)
            target[3:] *= float(self.rotation_range)
            final_target =  Transform.from_parameters(*target)

            #initial_pose_transform = Transform.from_parameters(*initial_pose)
            predicted_pose = combine_view_transform(Transform.from_parameters(*initial_pose), final_prediction)
            ground_truth_pose = combine_view_transform(Transform.from_parameters(*initial_pose), final_target)

            pose_diff_t, pose_diff_r = eval_pose_error(ground_truth_pose.matrix.reshape(1, 16),
                                                       predicted_pose.matrix.reshape(1, 16))
            #error = np.vstack((error, np.array([pose_diff_t,pose_diff_r])))
            error = np.append(error, np.array([[np.asscalar(pose_diff_t), np.asscalar(pose_diff_r)]]), axis=0)

        return error

    def train(self, epoch):
        """
        Minibatch loop for training. Will iterate through the whole dataset and backprop for every minibatch

        It will keep an average of the computed losses and every score obtained with the user's callbacks.

        The information is displayed on the console

        :return: averageloss, [averagescore1, averagescore2, ...]
        """
        batch_time = AverageMeter()
        data_time = AverageMeter()
        losses = AverageMeter()
        end = time.time()

        self.model.train()
        self.error=np.empty((0,2), float)

        if (self.scheduler is not None) and type(self.scheduler) == torch.optim.lr_scheduler.CyclicLR:
            self.use_cyclic_lr = True

        for i, (data, target) in tqdm(enumerate(self.train_data), total=len(self.train_data)):
            data_time.update(time.time() - end)
            data_org, target = self.setup_loaded_data(data, target, self.backend)
            data = data_org[0:-2]
            initial_pose=data_org[-2]
            data_var, target_var = self.to_autograd(data, target, is_train=True)
            y_pred = self.predict(data_var)
            loss = self.model.loss(y_pred, target_var)
            losses.update(loss.item(), data[0].size(0))

            target_numpy = target_var[0].cpu().numpy()
            target_numpy[:,:3] *= self.translation_range
            target_numpy[:, 3:] *= self.rotation_range
            target_numpy[:, 3:] = np.degrees(target_numpy[:, 3:])
            # import matplotlib.pyplot as plt
            # plt.figure("losses")
            # plt.clf()
            # plt.plot(losses.arr)
            # plt.show(block=False)
            # plt.pause(0.000001)
            # y_pred_numpy = y_pred[0].cpu.numpy()
            # target_numpy = target_var[0].cpu.numpy()
            delta_component = (y_pred[0].cpu().detach().numpy()- target_var[0].cpu().detach().numpy()) ** 2
            delta_mean= delta_component.mean(1)
            sample_loss = np.hstack((delta_component, np.reshape(delta_mean,(delta_component.shape[0],1))))

            error = self.calculate_error(initial_pose.detach().clone(),y_pred[0].detach().clone(),target[0].detach().clone())
            self.error = np.vstack((self.error, error))

            for i, callback in enumerate(self.callbacks):
                callback.batch(y_pred, data, target, sample_loss, target_numpy, is_train=True, tensorboard_logger=self.tensorboard_logger)

            [optim.zero_grad() for optim in self.optims]
            loss.backward()
            if self.gradient_clip:
                torch.nn.utils.clip_grad_norm(self.model.parameters(), 1)
            [optim.step() for optim in self.optims]

            if self.use_cyclic_lr:
                self.scheduler.step()
                #print('Batch-{0} lr: {1}'.format(epoch, self.optims[0].param_groups[0]['lr']))

            batch_time.update(time.time() - end)
            end = time.time()
        error_t=np.mean(self.error, axis=0)[0]
        error_r = np.mean(self.error, axis=0)[1]


        for i, callback in enumerate(self.callbacks):
            callback.epoch(epoch, losses.avg, data_time.avg, batch_time.avg, error_t, error_r,
                           is_train=True, tensorboard_logger=self.tensorboard_logger)

        return losses

    def validate(self, epoch, evaluate_mode=False):
        """
        Validation loop (refer to train())

        Only difference is that there is no backpropagation..

        #TODO: It repeats mostly train()'s code...

        :return:
        """
        batch_time = AverageMeter()
        data_time = AverageMeter()
        losses = AverageMeter()

        self.model.eval()
        self.error = np.empty((0, 2), float)
        self.log_wrong_samples=True
        end = time.time()
        for i, (data, target) in tqdm(enumerate(self.valid_data), total=len(self.valid_data)):
            data_time.update(time.time() - end)
            data_org, target = self.setup_loaded_data(data, target, self.backend)
            data = data_org[0:-2]
            initial_pose = data_org[-2]
            #index=data_org[-1]
            # print('index :', index)
            data_var, target_var = self.to_autograd(data, target, is_train=False)
            with torch.no_grad():
                y_pred = self.predict(data_var)
                loss = self.model.loss(y_pred, target_var)
            losses.update(loss.item(), data[0].size(0))

            target_numpy = target_var[0].cpu().numpy()
            target_numpy[:,:3] *= self.translation_range
            target_numpy[:, 3:] *= self.rotation_range
            target_numpy[:, 3:] = np.degrees(target_numpy[:, 3:])

            delta_component = (y_pred[0].cpu().detach().numpy()- target_var[0].cpu().detach().numpy()) ** 2
            delta_mean= delta_component.mean(1)
            sample_loss = np.hstack((delta_component, np.reshape(delta_mean,(delta_component.shape[0],1))))

            # data_prev0=data[0][0]
            # data_after0=data[1][0]
            #if self.log_wrong_samples:



            error = self.calculate_error(initial_pose.detach().clone(),y_pred[0].detach().clone(),target[0].detach().clone())
            self.error = np.vstack((self.error, error))

            for i, callback in enumerate(self.callbacks):
                callback.batch(y_pred, data, target, sample_loss, target_numpy, is_train=False, tensorboard_logger=self.tensorboard_logger)

            batch_time.update(time.time() - end)
            end = time.time()

        error_t=np.mean(self.error, axis=0)[0]
        error_r = np.mean(self.error, axis=0)[1]

        for i, callback in enumerate(self.callbacks):
            callback.epoch(epoch, losses.avg, data_time.avg, batch_time.avg, error_t, error_r, is_train=False,
                           tensorboard_logger=self.tensorboard_logger)

        print('valid epoch ' + str(epoch) + ' Avg loss is ' + str(losses.avg))
        print('valid epoch ' + str(epoch) + ' Avg pose_diff_t is ' + str(error_t))
        print('valid epoch ' + str(epoch) + ' Avg pose_diff_r is ' + str(error_r))

        if evaluate_mode:
            return losses.avg,error_t,error_r
        else:
            return losses

    def test(self, epoch, evaluate_mode=False):
        """
        Validation loop (refer to train())

        Only difference is that there is no backpropagation..

        #TODO: It repeats mostly train()'s code...

        :return:
        """

        losses = AverageMeter()
        errors_translation=AverageMeter()
        errors_rotation = AverageMeter()

        self.model.eval()

        end = time.time()
        for i in range(0, len(self.test_data)-1, 2):
            rgbA, depthA, rgbB, depthB, initial_pose, transformed_pose = self.test_data.get_raw_from_index(i)

            target_tensor = []
            Data0 = []
            Data1 = []
            for k in range(2):
                data, target = self.test_data[i + k]
                target_tensor.append(torch.from_numpy(target[0]).float())

                if k == 0:
                    Data0 = data[0].view((1, 4, 174, 174))
                    Data1 = data[1].view((1, 4, 174, 174))
                else:
                    Data0 = np.vstack((Data0, data[0].view((1, 4, 174, 174))))
                    Data1 = np.vstack((Data1, data[1].view((1, 4, 174, 174))))

            dataBatch = []
            dataBatch.append(torch.from_numpy(Data0).float().to('cuda'))
            dataBatch.append(torch.from_numpy(Data1).float().to('cuda'))

            targetBatch = []
            targetBatch.append(torch.from_numpy(
                np.vstack((target_tensor[0].view(1, 6).cpu(), target_tensor[1].view(1, 6).cpu()))).float().to(
                'cuda'))
            # targetBatch.append(torch.from_numpy(
            #     np.vstack((target_tensor[0].view(1, 6), target_tensor[1].view(1, 6)))).float())
            # targetBatch.append(target_tensor[0].view(1, 6).to('cuda'))
            # targetBatch.append(target_tensor[1].view(1, 6).to('cuda'))

            data_var, target_var = self.to_autograd(dataBatch, targetBatch, is_train=False)

            with torch.no_grad():
                y_pred = self.predict(data_var)
                loss = self.model.loss(y_pred, target_var)
            losses.update(loss.item(), 2) #data[0].size(0))
            squared_loss = np.square(y_pred[0][0,:].cpu()-target_var[0][0,:].cpu())

            predict_data = y_pred[0][0, :].cpu()
            predict_data[:3] *= float(self.translation_range)
            predict_data[3:] *= float(self.rotation_range)


            final_prediction = Transform.from_parameters(*predict_data)

            current_pose = combine_view_transform(initial_pose, final_prediction)
            current_pose_ground_truth = combine_view_transform(initial_pose, transformed_pose)

            pose_diff_t, pose_diff_r = eval_pose_error(current_pose_ground_truth.matrix.reshape(1, 16),
                                                       current_pose.matrix.reshape(1, 16))
            #print('img '+str(i) +" errot t and r are "+str(pose_diff_t)+","+str(pose_diff_r))
            errors_translation.update(pose_diff_t)
            errors_rotation.update(pose_diff_r)
            renderedRGB, renderedDepth = self.vpRender.render_image(current_pose,
                                                                    fbo_index=0,
                                                                    light_direction=self.sphere_sampler.random_direction(),
                                                                    light_diffuse=self.light_intensity,
                                                                    ambiant_light=self.ambiant_light)
            DeepTrackCallback.draw_debug_info(self, renderedRGB, current_pose, current_pose_ground_truth, self.camera)

            bb = compute_2Dboundingbox(initial_pose, self.camera, self.OBJECT_WIDTH, scale=(1000, -1000, -1000))
            renderedRGB, renderedDepth = normalize_scale(renderedRGB, renderedDepth, bb, self.IMAGE_SIZE)

            transformed_pose_para = transformed_pose.to_parameters().astype(np.float32)
            transformed_pose_para[3] = np.degrees(transformed_pose_para[3])
            transformed_pose_para[4] = np.degrees(transformed_pose_para[4])
            transformed_pose_para[5] = np.degrees(transformed_pose_para[5])

            for idx, callback in enumerate(self.callbacks):
                callback.log(rgbA, depthA, rgbB, depthB, renderedRGB, renderedDepth, epoch, i, loss.cpu().numpy(), squared_loss.cpu().numpy(), transformed_pose_para,
                             losses.avg, errors_translation.avg, errors_rotation.avg, tensorboard_logger=self.tensorboard_logger)

        if self.tensorboard_logger:
            self.tensorboard_logger.scalar_summary('test_error_t', errors_translation.avg, epoch + 1, is_test=True)
            self.tensorboard_logger.scalar_summary('test_error_r', errors_rotation.avg, epoch + 1, is_test=True)
            self.tensorboard_logger.scalar_summary('test_loss_avg', losses.avg, epoch + 1, is_test=True)

        print('test epoch ' + str(epoch) + ' Avg loss is ' + str(losses.avg))
        print('test epoch ' + str(epoch) + ' Avg pose_diff_t is ' + str(errors_translation.avg))
        print('test epoch ' + str(epoch) + ' Avg pose_diff_r is ' + str(errors_rotation.avg))

        if evaluate_mode:
            return losses.avg, errors_translation.avg, errors_rotation.avg
        else:
            return losses

    @staticmethod
    def load_checkpoint(path="", filename='checkpoint*.pth.tar'):
        """
        Helper function to load models's parameters
        :param state:   dict with metadata and models's weight
        :param path:    load path
        :param filename:string
        :return:
        """
        file_path = os.path.join(path, filename)
        print("Loading model...")
        state = torch.load(file_path)
        dict = state['state_dict']
        best_prec1 = state['best_prec1']
        epoch = state['epoch'] - 1
        return dict, best_prec1, epoch

    def loop(self, epochs_qty, output_path,
             load_best_checkpoint=False,
             load_last_checkpoint=False,
             save_best_checkpoint=True,
             save_last_checkpoint=True,
             save_all_checkpoints=True):
        """
        Training loop for n epoch.
        todo : Use callback instead of hardcoded savetxt to leave the user choise on results handling
        :param load_best_checkpoint:  If true, will check for model_best.pth.tar in output path and load it.
        :param save_best_checkpoint:  If true, will save model_best.pth.tar in output path.
        :param save_all_checkpoints:  If true, will save all checkpoints as checkpoint<epoch>.pth.tar in output path.
        :param epochs_qty:            Number of epoch to train
        :param output_path:           Path to save the model and log data
        :return:
        """
        best_prec1 = float('Inf')
        epoch_start = 0

        assert not (load_best_checkpoint and load_last_checkpoint), 'Choose to load only one model: last or best'
        if load_best_checkpoint or load_last_checkpoint:
            model_name = 'model_best.pth.tar' if load_best_checkpoint else 'model_last.pth.tar'
            if os.path.exists(os.path.join(output_path, model_name)):
                dict, best_prec1, epoch_best = self.load_checkpoint(output_path, model_name)
                self.model.load_state_dict(dict)
                # also get back the last i_epoch, won't start from 0 again
                epoch_start = epoch_best + 1
            else:
                raise RuntimeError("Can't load model {}".format(os.path.join(output_path, model_name)))

        for epoch in range(epoch_start, epochs_qty):
            print("-" * 20)
            print(" * EPOCH : {}".format(epoch))
            train_loss = self.train(epoch + 1)
            val_loss = self.validate(epoch + 1)
            test_loss = self.test(epoch + 1)

            if self.tensorboard_logger is not None:
                self.tensorboard_logger.scalar_summary('lr', self.optims[0].param_groups[0]['lr'], epoch + 1)

            if (self.scheduler is not None) and type(self.scheduler) != torch.optim.lr_scheduler.CyclicLR:
                print('Epoch-{0} lr: {1}'.format(epoch, self.optims[0].param_groups[0]['lr']))
                self.scheduler.step() if type(
                    self.scheduler) != torch.optim.lr_scheduler.ReduceLROnPlateau else self.scheduler.step(val_loss.val)
            else:
                print('Epoch-{0} lr: {1}'.format(epoch, self.optims[0].param_groups[0]['lr']))


            validation_loss_average = val_loss.avg
            training_loss_average = train_loss.avg

            if self.tensorboard_logger is not None:
                self.tensorboard_logger.scalar_summary('loss', training_loss_average, epoch + 1)
                self.tensorboard_logger.scalar_summary('loss', validation_loss_average, epoch + 1, is_train=False)
                for tag, value in self.model.named_parameters():
                    tag = tag.replace('.', '/')
                    self.tensorboard_logger.histo_summary(tag, self.to_np(value), epoch + 1)
                    try:
                        self.tensorboard_logger.histo_summary(tag + '/grad', self.to_np(value.grad), epoch + 1)
                    except AttributeError:
                        print('None grad:', tag, value.shape)
                        self.tensorboard_logger.histo_summary(tag + '/grad', np.asarray([0]), epoch + 1)

            # remember best loss and save checkpoint
            is_best = validation_loss_average < best_prec1
            best_prec1 = min(validation_loss_average, best_prec1)
            checkpoint_data = {'epoch': epoch, 'state_dict': self.model.state_dict(), 'best_prec1': best_prec1}
            if save_all_checkpoints:
                torch.save(checkpoint_data, os.path.join(output_path, "checkpoint{}.pth.tar".format(epoch)))
            if save_best_checkpoint and is_best:
                torch.save(checkpoint_data, os.path.join(output_path, "model_best.pth.tar"))
            if save_last_checkpoint:
                torch.save(checkpoint_data, os.path.join(output_path, "model_last.pth.tar"))

    @staticmethod
    def to_np(x):
        return x.data.cpu().numpy()
